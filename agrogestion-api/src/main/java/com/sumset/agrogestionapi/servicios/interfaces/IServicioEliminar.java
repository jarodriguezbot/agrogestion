/**
 * 
 */
package com.sumset.agrogestionapi.servicios.interfaces;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.RespuestaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;

/**
 * @author Sumset
 *
 */
public interface IServicioEliminar {

	/**
	 * Método para eliminar un centro de costo de la BD
	 * 
	 * @param centroDeCostoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una compra
	 * 
	 * @param comprasDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCompra(ComprasDTO comprasDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una cuenta contable
	 * 
	 * @param cuentaContableDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un descuento de nómina
	 * 
	 * @param descuentoNominaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una empresa
	 * 
	 * @param empresaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un funcionario por centro de costo
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarFuncionarioPorCentroCosto(FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO)
			throws AGExcepciones;

	/**
	 * Método para eliminar un funcionario
	 * 
	 * @param funcionarioDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un insumo por lote
	 * 
	 * @param insumoPorLoteDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un insumo
	 * 
	 * @param insumoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarInsumo(InsumoDTO insumoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una labor
	 * 
	 * @param laboresDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarLabores(LaboresDTO laboresDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un lote
	 * 
	 * @param loteDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarLote(LoteDTO loteDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un movimiento contable
	 * 
	 * @param movimientosContablesDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un recurso
	 * 
	 * @param recursoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarRecurso(RecursoDTO recursoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una referencia
	 * 
	 * @param referenciaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una venta
	 * 
	 * @param ventasDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarVentas(VentasDTO ventasDTO) throws AGExcepciones;

}
