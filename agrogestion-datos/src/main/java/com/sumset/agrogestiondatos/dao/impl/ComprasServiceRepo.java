/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IComprasServiceRepo;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IComprasRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class ComprasServiceRepo 
	extends OperacionesBasicasImpl<Compras, Long> implements IComprasServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IComprasRepo comprasRepo;
	
	@Override
	public CrudRepository<Compras, Long> getRepo() {
		return comprasRepo;
	}

	

}
