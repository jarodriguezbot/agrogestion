/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_insumo")
public class Insumo {

	/**
	 * 
	 */
	public Insumo() {
		super();
	}

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ism_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "ism_nombre", length = 255)
	private String ismNombre;
	
	@Basic(optional = false)
	@Column(name = "ism_unidad", length = 10)
	private String ismUnidad;
	
	@Basic(optional = false)
	@Column(name = "ism_precio")
	private Double ismPrecio;
}
