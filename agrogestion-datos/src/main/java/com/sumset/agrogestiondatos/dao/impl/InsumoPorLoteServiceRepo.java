/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IInsumoPorLoteServiceRepo;
import com.sumset.agrogestiondatos.models.InsumoPorLote;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IInsumoPorLoteRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class InsumoPorLoteServiceRepo 
	extends OperacionesBasicasImpl<InsumoPorLote, Long> implements IInsumoPorLoteServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IInsumoPorLoteRepo insumoLoteRepo;
	
	@Override
	public CrudRepository<InsumoPorLote, Long> getRepo() {
		return insumoLoteRepo;
	}


}
