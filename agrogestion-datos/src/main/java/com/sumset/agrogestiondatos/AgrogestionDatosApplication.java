package com.sumset.agrogestiondatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgrogestionDatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgrogestionDatosApplication.class, args);
	}

}
