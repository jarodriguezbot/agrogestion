/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.ILaboresServiceRepo;
import com.sumset.agrogestiondatos.models.Labores;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.ILaboresRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class LaboresServiceRepo 
	extends OperacionesBasicasImpl<Labores, Long> implements ILaboresServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ILaboresRepo laboresRepo;
	
	@Override
	public CrudRepository<Labores, Long> getRepo() {
		return laboresRepo;
	}

}
