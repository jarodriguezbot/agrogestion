/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IInsumoServiceRepo;
import com.sumset.agrogestiondatos.models.Insumo;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IInsumoRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class InsumoServiceRepo 
	extends OperacionesBasicasImpl<Insumo, Long> implements IInsumoServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IInsumoRepo insumoRepo;
	
	@Override
	public CrudRepository<Insumo, Long> getRepo() {
		return insumoRepo;
	}


}
