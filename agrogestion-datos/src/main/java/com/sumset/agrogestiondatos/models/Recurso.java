/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */@Data
 @Entity
 @Table(name = "ag_recurso")
public class Recurso {

	/**
	 * 
	 */
	public Recurso() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rec_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "rec_nombre", length = 255)
	private String recNombre;
	
	@Basic(optional = false)
	@Column(name = "rec_descripcion", length = 255)
	private String recDescripcion;
	
	@Basic(optional = false)
	@Column(name = "rec_unidad", length = 20)
	private String recUnidad;
	
	@Basic(optional = false)
	@Column(name = "rec_precio")
	private Double recPrecio;
	
	@JoinColumn(name = "rec_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centroCosto;
}
