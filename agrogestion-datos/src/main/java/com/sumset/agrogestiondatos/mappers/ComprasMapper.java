/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.sql.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Compras;

/**
 * @author joals
 *
 */
@Mapper(uses = {CentroDeCosto.class})
public interface ComprasMapper {
	
	/**
	 * 
	 * @param compras
	 * @return
	 */
	@Mapping(source = "compras.id", target = "id")
	@Mapping(source = "compras.cmpComprobante", target = "cmpComprobante")
	@Mapping(source = "compras.cmpFecha", target = "cmpFecha")
	@Mapping(source = "compras.cmpCorte", target = "cmpCorte")
	@Mapping(source = "compras.cmpTercero", target = "cmpTercero")
	@Mapping(source = "compras.cmpDescripcion", target = "cmpDescripcion")
	@Mapping(source = "compras.cmpCantidad", target = "cmpCantidad")
	@Mapping(source = "compras.cmpUnidad", target = "cmpUnidad")
	@Mapping(source = "compras.cmpRecurso", target = "cmpRecurso")
	@Mapping(source = "compras.cmpPrecio", target = "cmpPrecio")
	@Mapping(source = "compras.cmpDebito", target = "cmpDebito")
	@Mapping(source = "compras.cmpValorTotal", target = "cmpValorTotal")
	@Mapping(source = "compras.cmpCodDebito", target = "cmpCodDebito")
	@Mapping(source = "compras.cmpTipoGastDebito", target = "cmpTipoGastDebito")
	@Mapping(source = "compras.cmpTipoGastCredito", target = "cmpTipoGastCredito")
	@Mapping(source = "compras.centroCosto.id", target = "cmpCcId")
	ComprasDTO comprasADTO(Compras compras);
	
	/**
	 * 
	 * @param compras
	 * @return
	 */
	@Mapping(source = "compras.id", target = "id")
	@Mapping(source = "compras.cmpComprobante", target = "cmpComprobante")
	@Mapping(source = "compras.cmpFecha", target = "cmpFecha")
	@Mapping(source = "compras.cmpCorte", target = "cmpCorte")
	@Mapping(source = "compras.cmpTercero", target = "cmpTercero")
	@Mapping(source = "compras.cmpDescripcion", target = "cmpDescripcion")
	@Mapping(source = "compras.cmpCantidad", target = "cmpCantidad")
	@Mapping(source = "compras.cmpUnidad", target = "cmpUnidad")
	@Mapping(source = "compras.cmpRecurso", target = "cmpRecurso")
	@Mapping(source = "compras.cmpPrecio", target = "cmpPrecio")
	@Mapping(source = "compras.cmpDebito", target = "cmpDebito")
	@Mapping(source = "compras.cmpValorTotal", target = "cmpValorTotal")
	@Mapping(source = "compras.cmpCodDebito", target = "cmpCodDebito")
	@Mapping(source = "compras.cmpTipoGastDebito", target = "cmpTipoGastDebito")
	@Mapping(source = "compras.cmpTipoGastCredito", target = "cmpTipoGastCredito")
	@Mapping(source = "compras.cmpCcId", target = "centroCosto.id")
	Compras comprasDTOAEntidad(ComprasDTO compras);
	
	/**
	 * 
	 * @param compras
	 * @return
	 */
	List<ComprasDTO> comprasEntidadesADTO(List<Compras> compras);
	
	/**
	 * 
	 * @param comprasDTO
	 * @return
	 */
	List<Compras> comprasDTOAEntidades(List<ComprasDTO> comprasDTO);

}
