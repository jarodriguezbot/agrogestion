/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_labores")
public class Labores {

	/**
	 * 
	 */
	public Labores() {
		super();
	}

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "lab_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "lab_mayor_labor", length = 255)
	private Long labMayorLabor;
	
	@Basic(optional = false)
	@Column(name = "lab_fecha")
	private Date labFecha;
	
	@Basic(optional = false)
	@Column(name = "lab_precio")
	private Double labPrecio;
	
	@Basic(optional = false)
	@Column(name = "lab_cuenta")
	private Long labCuenta;
	
	@Basic(optional = false)
	@Column(name = "lab_comprobante")
	private Long labComprobante;
	
	@Basic(optional = false)
	@Column(name = "lab_valor_total")
	private Double labValorTotal;
	
	@Basic(optional = false)
	@Column(name = "lab_estado", length = 20)
	private String labEstado;
	
	@Basic(optional = false)
	@Column(name = "lab_cultivo")
	private Long labCultivo;
	
	@JoinColumn(name = "lab_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centroCosto;
}
