/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.CuentaContable;

/**
 * @author joals
 *
 */
@Mapper
public interface CuentaContableMapper {
	
	/**
	 * 
	 * @param cuentaContable
	 * @return
	 */
	@Mapping(source = "cuentaContable.id", target = "id")
	@Mapping(source = "cuentaContable.ctaNombre", target = "ctaNombre")
	@Mapping(source = "cuentaContable.ctaCodDebito", target = "ctaCodDebito")
	@Mapping(source = "cuentaContable.ctaCodCredito", target = "ctaCodCredito")
	@Mapping(source = "cuentaContable.ctaTipo", target = "ctaTipo")
	@Mapping(source = "cuentaContable.ctaPuc", target = "ctaPuc")
	@Mapping(source = "cuentaContable.ctaUso", target = "ctaUso")
	@Mapping(source = "cuentaContable.ctaGrupo", target = "ctaGrupo")
	CuentaContableDTO cuentaContableADTO(CuentaContable cuentaContable);
	
	/**
	 * 
	 * @param cuentaContable
	 * @return
	 */
	@Mapping(source = "cuentaContable.id", target = "id")
	@Mapping(source = "cuentaContable.ctaNombre", target = "ctaNombre")
	@Mapping(source = "cuentaContable.ctaCodDebito", target = "ctaCodDebito")
	@Mapping(source = "cuentaContable.ctaCodCredito", target = "ctaCodCredito")
	@Mapping(source = "cuentaContable.ctaTipo", target = "ctaTipo")
	@Mapping(source = "cuentaContable.ctaPuc", target = "ctaPuc")
	@Mapping(source = "cuentaContable.ctaUso", target = "ctaUso")
	@Mapping(source = "cuentaContable.ctaGrupo", target = "ctaGrupo")
	CuentaContable cuentaContableDTOAEntidad(CuentaContableDTO cuentaContable);
	
	/**
	 * 
	 * @param cuentaContables
	 * @return
	 */
	List<CuentaContableDTO> cuentaContableEntidadesADTO(List<CuentaContable> cuentaContables);
	
	/**
	 * 
	 * @param cuentaContablesDTO
	 * @return
	 */
	List<CuentaContable> cuentaContableDTOAEntidades(List<CuentaContableDTO> cuentaContablesDTO);
	
}
