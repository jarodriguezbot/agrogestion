/**
 * 
 */
package com.sumset.agrogestiondatos.dao.interfaces;

import java.io.Serializable;

import com.sumset.agrogestiondatos.models.Insumo;
import com.sumset.agrogestiondatos.operacionesbasicas.interfaces.IOperacionesBasicas;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IInsumoServiceRepo extends IOperacionesBasicas<Insumo, Long>, Serializable{

}
