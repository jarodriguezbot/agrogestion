/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_fun_x_cc")
public class FuncionarioPorCentroCosto {

	/**
	 * 
	 */
	public FuncionarioPorCentroCosto() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fxc_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "fxc_estado", length = 255)
	private String fxcEstado;
	
	@JoinColumn(name = "fxc_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centroCosto;

	@JoinColumn(name = "fxc_fun_id", referencedColumnName = "fun_id")
	@ManyToOne
	private Funcionario fxcFunId;
}
