/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Ventas;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IVentasRepo extends CrudRepository<Ventas, Long>{

}
