/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_insumo_x_lote")
public class InsumoPorLote {

	/**
	 * 
	 */
	public InsumoPorLote() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ixl_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "ixl_fecha")
	private Date ixlFecha;
	
	@Basic(optional = false)
	@Column(name = "ixl_corte")
	private Long ixlCorte;
	
	@Basic(optional = false)
	@Column(name = "ixl_unidad", length = 10)
	private String ixlUnidad;

	@Basic(optional = false)
	@Column(name = "ixl_cantidad")
	private Long ixlCantidad;
	
	@Basic(optional = false)
	@Column(name = "ixl_precio")
	private Double ixlPrecio;
	
	@Basic(optional = false)
	@Column(name = "ixl_valor_total")
	private Double ixlValorTotal;
	
	@Basic(optional = false)
	@Column(name = "ixl_creditos")
	private Double ixlCreditos;
	
	@Basic(optional = false)
	@Column(name = "ixl_comprobante", length = 255)
	private String ixlComprobante;
	
	@Basic(optional = false)
	@Column(name = "ixl_num_cortes")
	private Long ixlNumeroCortes;
	
	@Basic(optional = false)
	@Column(name = "ixl_estado_cultivo", length = 255)
	private String ixlEstadoCultivo;
	
	@Basic(optional = false)
	@Column(name = "ixl_cultivo")
	private Long ixlCultivo;
	
	@Basic(optional = false)
	@Column(name = "ixl_mayor_labores", length = 255)
	private String ixlMayorLabores;
	
	@JoinColumn(name = "ixl_ins_id", referencedColumnName = "ins_id")
	@ManyToOne
	private Insumo ixlInsId;
	
	@JoinColumn(name = "ixl_lote_id", referencedColumnName = "lot_id")
	@ManyToOne
	private Lote ixlLoteId;
}
