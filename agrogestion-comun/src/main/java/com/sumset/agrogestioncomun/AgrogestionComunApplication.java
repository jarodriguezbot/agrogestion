package com.sumset.agrogestioncomun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgrogestionComunApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgrogestionComunApplication.class, args);
	}

}
