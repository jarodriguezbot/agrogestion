/**
 * 
 */
package com.sumset.agrogestionapi.controladores.interfaces;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.GenericoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.RespuestaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;

/**
 * @author Sumset
 *
 */
public interface IAGControlador {
	/**
	 * Método que nos permite consultar todos los centros de costo
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosCentroCostos() throws AGExcepciones;

	/**
	 * Método que retorna un centro de costo por Id
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO buscarCentroCostoPorId(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método que guarda un centro de costo en la BD
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO guardarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método para modificar un centro de costo
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO modificarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un centro de costo de la BD
	 * 
	 * @param centroDeCostoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las compras
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasCompras() throws AGExcepciones;

	/**
	 * Método para buscar compras por Id
	 * 
	 * @param comprasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ComprasDTO buscarCompraPorId(ComprasDTO comprasDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una compra
	 * 
	 * @param comprasDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCompra(ComprasDTO comprasDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una compra
	 * 
	 * @param comprasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ComprasDTO guardarCompra(ComprasDTO comprasDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las cuentas contables
	 * 
	 * @return lista con todas las cuentas GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasCuentasContables() throws AGExcepciones;

	/**
	 * Método para buscar cuenta contable por Id
	 * 
	 * @param cuentaContableDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CuentaContableDTO buscarCuentaContablePorId(CuentaContableDTO cuentaContableDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una cuenta contable
	 * 
	 * @param cuentaContableDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una cuenta contable
	 * 
	 * @param cuentaContableDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CuentaContableDTO guardarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los descuentos de nómina
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosDescuentosNomina() throws AGExcepciones;

	/**
	 * Método para buscar un descuento de nómina por Id
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public DescuentoNominaDTO buscarDescuentoNominaPorId(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un descuento de nómina
	 * 
	 * @param descuentoNominaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un descuento de nómina
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public DescuentoNominaDTO guardarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las empresas
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasEmpresas() throws AGExcepciones;

	/**
	 * Método para buscar empresas por Id
	 * 
	 * @param empresaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public EmpresaDTO buscarEmpresaPorId(EmpresaDTO empresaDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una empresa
	 * 
	 * @param empresaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una empresa
	 * 
	 * @param empresaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public EmpresaDTO guardarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los funcionarios por Centro de Costo
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosFuncionariosPorCentroCosto() throws AGExcepciones;

	/**
	 * Método para buscar un funcionario por centro de costo por Id
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioPorCentroCostoDTO buscarFuncionarioPorCentroCostoPorId(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un funcionario por centro de costo
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarFuncionarioPorCentroCosto(FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO)
			throws AGExcepciones;

	/**
	 * Método para agregar o modificar un funcionario por centro de costo
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioPorCentroCostoDTO guardarFuncionarioPorCentroCosto(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los funcionarios
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosFuncionarios() throws AGExcepciones;

	/**
	 * Método para buscar un funcionario por Id
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioDTO buscarFuncionarioPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un funcionario
	 * 
	 * @param funcionarioDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un funcionario
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioDTO guardarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los insumos por lote
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosInsumosPorLote() throws AGExcepciones;

	/**
	 * Método para buscar los insumos por lote por Id
	 * 
	 * @param insumoPorLoteDTO
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public InsumoPorLoteDTO buscarInsumosPorLotePorId(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un insumo por lote
	 * 
	 * @param insumoPorLoteDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un lote
	 * 
	 * @param insumoPorLoteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoPorLoteDTO guardarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los insumos
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosInsumos() throws AGExcepciones;

	/**
	 * Método para buscar insumo por Id
	 * 
	 * @param insumoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoDTO buscarInsumosPorId(InsumoDTO insumoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un insumo
	 * 
	 * @param insumoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarInsumo(InsumoDTO insumoDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un insumo
	 * 
	 * @param insumoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoDTO guardarInsumo(InsumoDTO insumoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las labores
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasLabores() throws AGExcepciones;

	/**
	 * Método para buscar labores por Id
	 * 
	 * @param laboresDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LaboresDTO buscarLaboresPorId(LaboresDTO laboresDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una labor
	 * 
	 * @param laboresDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarLabores(LaboresDTO laboresDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una labor
	 * 
	 * @param laboresDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LaboresDTO guardarLabor(LaboresDTO laboresDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los lotes
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosLotes() throws AGExcepciones;

	/**
	 * Método para buscar lote por Id
	 * 
	 * @param loteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LoteDTO buscarLotePorId(LoteDTO loteDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un lote
	 * 
	 * @param loteDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarLote(LoteDTO loteDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un lote
	 * 
	 * @param loteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LoteDTO guardarLote(LoteDTO loteDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los movimientos contables
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosMovimientosContables() throws AGExcepciones;

	/**
	 * Método para buscar un movimiento contable por Id
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public MovimientosContablesDTO buscarMovimientosContablesPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un movimiento contable
	 * 
	 * @param movimientosContablesDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un movimiento contable
	 * 
	 * @param movimientosContablesDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public MovimientosContablesDTO guardarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones;

	/**
	 * Método para buscar todos los recursos
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosRecursos() throws AGExcepciones;

	/**
	 * Método para buscar un recurso por Id
	 * 
	 * @param recursoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public RecursoDTO buscarRecursoPorId(RecursoDTO recursoDTO) throws AGExcepciones;

	/**
	 * Método para eliminar un recurso
	 * 
	 * @param recursoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarRecurso(RecursoDTO recursoDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un recurso
	 * 
	 * @param recursoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public RecursoDTO guardarRecurso(RecursoDTO recursoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas la referencias
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasReferencias() throws AGExcepciones;

	/**
	 * Método para buscar referencia por Id
	 * 
	 * @param referenciaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ReferenciaDTO buscarReferenciaPorId(ReferenciaDTO referenciaDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una referencia
	 * 
	 * @param referenciaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una referencia
	 * 
	 * @param referenciaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ReferenciaDTO guardarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las ventas
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasVentas() throws AGExcepciones;

	/**
	 * Método para buscar una venta por Id
	 * 
	 * @param ventasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public VentasDTO buscarVentasPorId(VentasDTO ventasDTO) throws AGExcepciones;

	/**
	 * Método para eliminar una venta
	 * 
	 * @param ventasDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarVentas(VentasDTO ventasDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una venta
	 * 
	 * @param ventasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public VentasDTO guardarVenta(VentasDTO ventasDTO) throws AGExcepciones;


}
