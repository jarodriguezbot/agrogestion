/**
 * 
 */
package com.sumset.agrogestioncomun.utils;

import org.springframework.stereotype.Component;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Component
public class AGLogger {
	
	public static final AGLogger AGLOGGER = new AGLogger();

	/**
	 * 
	 */
	public AGLogger() {
		super();
	}
	
	public void messageLogger(String message, String severity, Class<?> claseName) {
		Logger logger = LogManager.getLogger(claseName);
		
		switch (severity) {
		case AGConstantes.SEVERIDAD_DEBUG:
			logger.debug(message);
			break;
		case AGConstantes.SEVERIDAD_ERROR:
			logger.error(message);
			break;
		case AGConstantes.SEVERIDAD_FATAL:
			logger.fatal(message);
			break;
		case AGConstantes.SEVERIDAD_INFO:
			logger.info(message);
			break;
		case AGConstantes.SEVERIDAD_TRACE:
			logger.trace(message);
			break;
		case AGConstantes.SEVERIDAD_WARN:
			logger.warn(message);
			break;
		default:
			logger.debug(message);
			break;
		}
	}
}
