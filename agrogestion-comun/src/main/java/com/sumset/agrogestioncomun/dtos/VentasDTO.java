/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.sql.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class VentasDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public VentasDTO() {
		super();
	}

	private Long id;
	private String vtaComprobante;
	private Date vtaFecha;
	private Long vtaCorte;
	private String vtaTercero;
	private String vtaCredito;
	private Long vtaCantidad;
	private String vtaUnidad;
	private Double vtaPrecio;
	private Double vtaValorTotal;
	private String vtaCodDebito;
	private String vtaEstadoCultivo;
	private long vtaCcId;
	
	
}
