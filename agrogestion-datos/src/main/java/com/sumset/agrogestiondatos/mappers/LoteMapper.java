/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Lote;

/**
 * @author joals
 *
 */
@Mapper(uses = {CentroDeCosto.class})
public interface LoteMapper {
	
	/**
	 * 
	 * @param lote
	 * @return
	 */
	@Mapping(source = "lote.id",  target = "id")
	@Mapping(source = "lote.lotNombre",  target = "lotNombre")
	@Mapping(source = "lote.lotCodigo",  target = "lotCodigo")
	@Mapping(source = "lote.lotArbolesContados",  target = "lotArbolesContados")
	@Mapping(source = "lote.lotAreaBruta",  target = "lotAreaBruta")
	@Mapping(source = "lote.lotUnidad",  target = "lotUnidad")
	@Mapping(source = "lote.lotAreaAgricola",  target = "lotAreaAgricola")
	@Mapping(source = "lote.lotCultivo",  target = "lotCultivo")
	@Mapping(source = "lote.lotChar",  target = "lotChar")
	@Mapping(source = "lote.lotAnioRenovacion",  target = "lotAnioRenovacion")
	@Mapping(source = "lote.lotEstado",  target = "lotEstado")
	@Mapping(source = "lote.lotCalculoHectarea",  target = "lotCalculoHectarea")
	@Mapping(source = "lote.lotPrecioUnitario",  target = "lotPrecioUnitario")
	@Mapping(source = "lote.lotProducProyectada",  target = "lotProducProyectada")
	@Mapping(source = "lote.lotFacturacion",  target = "lotFacturacion")
	@Mapping(source = "lote.lotObservaciones",  target = "lotObservaciones")
	@Mapping(source = "lote.centroCosto.id",  target = "lotCcId")
	LoteDTO loteADTO(Lote lote);
	
	/**
	 * 
	 * @param lote
	 * @return
	 */
	@Mapping(source = "lote.id",  target = "id")
	@Mapping(source = "lote.lotNombre",  target = "lotNombre")
	@Mapping(source = "lote.lotCodigo",  target = "lotCodigo")
	@Mapping(source = "lote.lotArbolesContados",  target = "lotArbolesContados")
	@Mapping(source = "lote.lotAreaBruta",  target = "lotAreaBruta")
	@Mapping(source = "lote.lotUnidad",  target = "lotUnidad")
	@Mapping(source = "lote.lotAreaAgricola",  target = "lotAreaAgricola")
	@Mapping(source = "lote.lotCultivo",  target = "lotCultivo")
	@Mapping(source = "lote.lotChar",  target = "lotChar")
	@Mapping(source = "lote.lotAnioRenovacion",  target = "lotAnioRenovacion")
	@Mapping(source = "lote.lotEstado",  target = "lotEstado")
	@Mapping(source = "lote.lotCalculoHectarea",  target = "lotCalculoHectarea")
	@Mapping(source = "lote.lotPrecioUnitario",  target = "lotPrecioUnitario")
	@Mapping(source = "lote.lotProducProyectada",  target = "lotProducProyectada")
	@Mapping(source = "lote.lotFacturacion",  target = "lotFacturacion")
	@Mapping(source = "lote.lotObservaciones",  target = "lotObservaciones")
	@Mapping(source = "lote.lotCcId",  target = "centroCosto.id")
	Lote loteDTOAEntidad(LoteDTO lote);
	
	/**
	 * 
	 * @param lotes
	 * @return
	 */
	List<LoteDTO> lotesEntidadesADTO(List<Lote> lotes);
	
	/**
	 * 
	 * @param lotes
	 * @return
	 */
	List<Lote> lotesDTOAEntidades(List<LoteDTO> lotes);
}
