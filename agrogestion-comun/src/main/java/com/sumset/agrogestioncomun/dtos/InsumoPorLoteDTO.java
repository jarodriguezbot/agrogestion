/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.sql.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class InsumoPorLoteDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public InsumoPorLoteDTO() {
		super();
	}
	
	private Long id;
	private Date ixlFecha;
	private Long ixlCorte;
	private String ixlUnidad;
	private Long ixlCantidad;
	private Double ixlPrecio;
	private Double ixlValorTotal;
	private Double ixlCreditos;
	private String ixlComprobante;
	private Long ixlNumeroCortes;
	private String ixlEstadoCultivo;
	private Long ixlCultivo;
	private String ixlMayorLabores;
	private Long ixlInsId;
	private Long ixlLoteId;

}
