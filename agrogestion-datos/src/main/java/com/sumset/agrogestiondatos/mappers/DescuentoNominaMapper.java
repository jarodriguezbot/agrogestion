/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestiondatos.models.DescuentoNomina;
import com.sumset.agrogestiondatos.models.Funcionario;
import com.sumset.agrogestiondatos.models.Lote;

/**
 * @author joals
 *
 */
@Mapper(uses = {Funcionario.class, Lote.class})
public interface DescuentoNominaMapper {
	
	/**
	 * 
	 * @param descuentoNomina
	 * @return
	 */
	@Mapping(source = "descuentoNomina.id",  target = "id")
	@Mapping(source = "descuentoNomina.dnoFecha",  target = "dnoFecha")
	@Mapping(source = "descuentoNomina.dnoCorte",  target = "dnoCorte")
	@Mapping(source = "descuentoNomina.dnoTipoDescuento",  target = "dnoTipoDescuento")
	@Mapping(source = "descuentoNomina.dnoNombreFuncionario",  target = "dnoNombreFuncionario")
	@Mapping(source = "descuentoNomina.dnoCedulaFuncionario",  target = "dnoCedulaFuncionario")
	@Mapping(source = "descuentoNomina.dnoCantidad",  target = "dnoCantidad")
	@Mapping(source = "descuentoNomina.dnoPrecio",  target = "dnoPrecio")
	@Mapping(source = "descuentoNomina.dnoValorTotal",  target = "dnoValorTotal")
	@Mapping(source = "descuentoNomina.dnoDebito",  target = "dnoDebito")
	@Mapping(source = "descuentoNomina.dnoComprobante",  target = "dnoComprobante")
	@Mapping(source = "descuentoNomina.dnoNumeroComprobante",  target = "dnoNumeroComprobante")
	@Mapping(source = "descuentoNomina.dnoFuncId.id",  target = "dnoFuncId")
	@Mapping(source = "descuentoNomina.dnoLoteId.id",  target = "dnoLoteId")
	DescuentoNominaDTO descuentoNominaADTO(DescuentoNomina descuentoNomina);
	
	/**
	 * 
	 * @param descuentoNomina
	 * @return
	 */
	@Mapping(source = "descuentoNomina.id",  target = "id")
	@Mapping(source = "descuentoNomina.dnoFecha",  target = "dnoFecha")
	@Mapping(source = "descuentoNomina.dnoCorte",  target = "dnoCorte")
	@Mapping(source = "descuentoNomina.dnoTipoDescuento",  target = "dnoTipoDescuento")
	@Mapping(source = "descuentoNomina.dnoNombreFuncionario",  target = "dnoNombreFuncionario")
	@Mapping(source = "descuentoNomina.dnoCedulaFuncionario",  target = "dnoCedulaFuncionario")
	@Mapping(source = "descuentoNomina.dnoCantidad",  target = "dnoCantidad")
	@Mapping(source = "descuentoNomina.dnoPrecio",  target = "dnoPrecio")
	@Mapping(source = "descuentoNomina.dnoValorTotal",  target = "dnoValorTotal")
	@Mapping(source = "descuentoNomina.dnoDebito",  target = "dnoDebito")
	@Mapping(source = "descuentoNomina.dnoComprobante",  target = "dnoComprobante")
	@Mapping(source = "descuentoNomina.dnoNumeroComprobante",  target = "dnoNumeroComprobante")
	@Mapping(source = "descuentoNomina.dnoFuncId",  target = "dnoFuncId.id")
	@Mapping(source = "descuentoNomina.dnoLoteId",  target = "dnoLoteId.id")
	DescuentoNomina descuentoNominaDTOAEntidad(DescuentoNominaDTO descuentoNomina);
	
	/**
	 * 
	 * @param descuentoNominas
	 * @return
	 */
	List<DescuentoNominaDTO> descuentosNominaEntidadesADTO(List<DescuentoNomina> descuentoNominas);
	
	/**
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 */
	List<DescuentoNomina> descuentosNominaDTOAEntidades(List<DescuentoNominaDTO> descuentoNominaDTO);

}
