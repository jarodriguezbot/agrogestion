/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.CuentaContable;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface ICuentaContableRepo extends CrudRepository<CuentaContable, Long>{

}
