/**
 * 
 */
package com.sumset.agrogestiondatos.dao.interfaces;

import java.io.Serializable;

import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;
import com.sumset.agrogestiondatos.operacionesbasicas.interfaces.IOperacionesBasicas;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IFuncionarioPorCentroCostoServiceRepo extends IOperacionesBasicas<FuncionarioPorCentroCosto, Long>, Serializable{

}
