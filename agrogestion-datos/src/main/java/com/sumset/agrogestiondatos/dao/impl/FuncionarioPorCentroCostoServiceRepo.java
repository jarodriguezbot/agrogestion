/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IFuncionarioPorCentroCostoServiceRepo;
import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class FuncionarioPorCentroCostoServiceRepo 
	extends OperacionesBasicasImpl<FuncionarioPorCentroCosto, Long> implements IFuncionarioPorCentroCostoServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	
	@Override
	public CrudRepository<FuncionarioPorCentroCosto, Long> getRepo() {
		return null;
	}


}
