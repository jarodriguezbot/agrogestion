/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.ICuentaContableServiceRepo;
import com.sumset.agrogestiondatos.models.CuentaContable;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.ICuentaContableRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class CuentaContableServiceRepo 
	extends OperacionesBasicasImpl<CuentaContable, Long> implements ICuentaContableServiceRepo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICuentaContableRepo cuentaContableRepo;
	@Override
	public CrudRepository<CuentaContable, Long> getRepo() {
		return cuentaContableRepo;
	}

}
