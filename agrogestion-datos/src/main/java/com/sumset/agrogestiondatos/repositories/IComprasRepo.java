/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Compras;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IComprasRepo extends CrudRepository<Compras, Long>{

}
