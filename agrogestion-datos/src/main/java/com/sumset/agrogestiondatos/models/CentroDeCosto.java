/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_cc")
public class CentroDeCosto {

	/**
	 * 
	 */
	public CentroDeCosto() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cc_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "cc_nombre", length = 255)
	private String ccNombre;
	
	@Basic(optional = false)
	@Column(name = "cc_descripcion", length = 255)
	private String ccDescripcion;
	
	@JoinColumn(name = "cc_emp_id", referencedColumnName = "emp_id")
	@ManyToOne
	private Empresa empresa;
}
