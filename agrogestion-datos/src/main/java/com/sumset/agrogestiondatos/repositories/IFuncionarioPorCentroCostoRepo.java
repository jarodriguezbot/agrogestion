/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IFuncionarioPorCentroCostoRepo extends CrudRepository<FuncionarioPorCentroCosto, Long>{

}
