/**
 * 
 */
package com.sumset.agrogestiondatos.serviciofacade;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sumset.agrogestioncomun.utils.AGLogger;
import com.sumset.agrogestiondatos.dao.interfaces.ICentroDeCostoServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IComprasServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.ICuentaContableServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IDescuentoNominaServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IEmpresaServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IFuncionarioPorCentroCostoServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IFuncionarioServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IInsumoPorLoteServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IInsumoServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.ILaboresServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.ILoteServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IMovimientosContablesServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IRecursoServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IReferenciaServiceRepo;
import com.sumset.agrogestiondatos.dao.interfaces.IVentasServiceRepo;
import com.sumset.agrogestiondatos.mappers.CentroCostoMapper;
import com.sumset.agrogestiondatos.mappers.ComprasMapper;
import com.sumset.agrogestiondatos.mappers.CuentaContableMapper;
import com.sumset.agrogestiondatos.mappers.DescuentoNominaMapper;
import com.sumset.agrogestiondatos.mappers.EmpresaMapper;
import com.sumset.agrogestiondatos.mappers.FuncionarioMapper;
import com.sumset.agrogestiondatos.mappers.FuncionarioPorCentroDeCostoMapper;
import com.sumset.agrogestiondatos.mappers.InsumoMapper;
import com.sumset.agrogestiondatos.mappers.InsumoPorLotesMapper;
import com.sumset.agrogestiondatos.mappers.LaboresMapper;
import com.sumset.agrogestiondatos.mappers.LoteMapper;
import com.sumset.agrogestiondatos.mappers.MovimientosContablesMapper;
import com.sumset.agrogestiondatos.mappers.RecursoMapper;
import com.sumset.agrogestiondatos.mappers.ReferenciaMapper;
import com.sumset.agrogestiondatos.mappers.VentasMapper;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Component
public class ServicioFacade implements Serializable {

	@Autowired
	ICentroDeCostoServiceRepo centroCostoDAO;
	
	@Autowired
	IComprasServiceRepo comprasDAO;
	
	@Autowired
	ICuentaContableServiceRepo cuentaContableDAO;
	
	@Autowired
	IDescuentoNominaServiceRepo descuentoNominaDAO;
	
	@Autowired
	IEmpresaServiceRepo empresaDAO;
	
	@Autowired
	IFuncionarioPorCentroCostoServiceRepo funcionarioPorCentroCostoDAO;
	
	@Autowired
	IFuncionarioServiceRepo funcionarioDAO;
	
	@Autowired
	IInsumoPorLoteServiceRepo insumoPorLoteDAO;
	
	@Autowired
	IInsumoServiceRepo insumoDAO;
	
	@Autowired
	ILaboresServiceRepo laboresDAO;
	
	@Autowired
	ILoteServiceRepo loteDAO;
	
	@Autowired
	IMovimientosContablesServiceRepo movimientosContablesDAO;
	
	@Autowired
	IRecursoServiceRepo recursoDAO;
	
	@Autowired
	IReferenciaServiceRepo referenciaDAO;
	
	@Autowired
	IVentasServiceRepo ventasDAO;
	
	@Autowired
	CentroCostoMapper centroCostoMapper;
	
	@Autowired
	ComprasMapper comprasMapper;
	
	@Autowired
	CuentaContableMapper cuentaContableMapper;
	
	@Autowired
	DescuentoNominaMapper descuentoNominaMapper;
	
	@Autowired
	EmpresaMapper empresaMapper;
	
	@Autowired
	FuncionarioMapper funcionarioMapper;
	
	@Autowired
	FuncionarioPorCentroDeCostoMapper funcionarioPorCentroCostoMapper;
	
	@Autowired
	InsumoMapper insumoMapper;
	
	@Autowired
	InsumoPorLotesMapper insumoPorLoteMapper;
	
	@Autowired
	LaboresMapper laboresMapper;
	
	@Autowired
	LoteMapper loteMapper;
	
	@Autowired
	MovimientosContablesMapper movimientosContablesMapper;
	
	@Autowired
	RecursoMapper recursoMapper;
	
	@Autowired
	ReferenciaMapper referenciaMapper;
	
	@Autowired
	VentasMapper ventasMapper;
	
	@Autowired
	AGLogger logger;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ServicioFacade() {
		super();
	}
}
