/**
 * 
 */
package com.sumset.agrogestionapi.servicios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sumset.agrogestionapi.servicios.interfaces.IServicioEliminar;
import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.RespuestaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGLogger;
import com.sumset.agrogestiondatos.sessionfacade.interfaces.ISessionFacade;

/**
 * @author Sumset
 *
 */
@Service
public class ServicioEliminar implements IServicioEliminar {

	@Autowired
	ISessionFacade sessionFacade;
	
	@Autowired
	AGLogger logger;
	
	/**
	 * 
	 */
	public ServicioEliminar() {
		super();
	}

	@Override
	public RespuestaDTO eliminarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		return sessionFacade.eliminarCentroCosto(centroDeCostoDTO); 
	}

	@Override
	public RespuestaDTO eliminarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		return sessionFacade.eliminarCompra(comprasDTO);
	}

	@Override
	public RespuestaDTO eliminarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		return sessionFacade.eliminarCuentaContable(cuentaContableDTO);
	}

	@Override
	public RespuestaDTO eliminarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		return sessionFacade.eliminarDescuentoNomina(descuentoNominaDTO);
	}

	@Override
	public RespuestaDTO eliminarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		return sessionFacade.eliminarEmpresa(empresaDTO);
	}

	@Override
	public RespuestaDTO eliminarFuncionarioPorCentroCosto(FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO)
			throws AGExcepciones {
		return sessionFacade.eliminarFuncionarioPorCentroCosto(funcionarioPorCentroCostoDTO);
	}

	@Override
	public RespuestaDTO eliminarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		return sessionFacade.eliminarFuncionario(funcionarioDTO);
	}

	@Override
	public RespuestaDTO eliminarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		return sessionFacade.eliminarInsumoPorLote(insumoPorLoteDTO);
	}

	@Override
	public RespuestaDTO eliminarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		return sessionFacade.eliminarInsumo(insumoDTO);
	}

	@Override
	public RespuestaDTO eliminarLabores(LaboresDTO laboresDTO) throws AGExcepciones {
		return sessionFacade.eliminarLabores(laboresDTO);
	}

	@Override
	public RespuestaDTO eliminarLote(LoteDTO loteDTO) throws AGExcepciones {
		return sessionFacade.eliminarLote(loteDTO);
	}

	@Override
	public RespuestaDTO eliminarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO) throws AGExcepciones {
		return sessionFacade.eliminarMovimientoContable(movimientosContablesDTO);
	}

	@Override
	public RespuestaDTO eliminarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		return sessionFacade.eliminarRecurso(recursoDTO);
	}

	@Override
	public RespuestaDTO eliminarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		return sessionFacade.eliminarReferencia(referenciaDTO);
	}

	@Override
	public RespuestaDTO eliminarVentas(VentasDTO ventasDTO) throws AGExcepciones {
		return sessionFacade.eliminarVentas(ventasDTO);
	}

}
