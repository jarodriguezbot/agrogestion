/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class EmpresaDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public EmpresaDTO() {
		super();
	}

	private Long id;
	private String empNombre;
	private String empDireccion;

}
