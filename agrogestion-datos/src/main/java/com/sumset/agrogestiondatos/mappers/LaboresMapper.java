/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Labores;

/**
 * @author joals
 *
 */
@Mapper(uses = {CentroDeCosto.class})
public interface LaboresMapper {
	
	/**
	 * 
	 * @param labores
	 * @return
	 */
	@Mapping(source = "labor.id",  target = "id")
	@Mapping(source = "labor.labMayorLabor",  target = "labMayorLabor")
	@Mapping(source = "labor.labFecha",  target = "labFecha")
	@Mapping(source = "labor.labPrecio",  target = "labPrecio")
	@Mapping(source = "labor.labCuenta",  target = "labCuenta")
	@Mapping(source = "labor.labComprobante",  target = "labComprobante")
	@Mapping(source = "labor.labValorTotal",  target = "labValorTotal")
	@Mapping(source = "labor.labEstado",  target = "labEstado")
	@Mapping(source = "labor.labCultivo",  target = "labCultivo")
	@Mapping(source = "labor.centroCosto.id",  target = "ccLabId")
	LaboresDTO laborADTO(Labores labor);
	
	/**
	 * 
	 * @param labores
	 * @return
	 */
	@Mapping(source = "labor.id",  target = "id")
	@Mapping(source = "labor.labMayorLabor",  target = "labMayorLabor")
	@Mapping(source = "labor.labFecha",  target = "labFecha")
	@Mapping(source = "labor.labPrecio",  target = "labPrecio")
	@Mapping(source = "labor.labCuenta",  target = "labCuenta")
	@Mapping(source = "labor.labComprobante",  target = "labComprobante")
	@Mapping(source = "labor.labValorTotal",  target = "labValorTotal")
	@Mapping(source = "labor.labEstado",  target = "labEstado")
	@Mapping(source = "labor.labCultivo",  target = "labCultivo")
	@Mapping(source = "labor.ccLabId",  target = "centroCosto.id")
	Labores laborDTOAEntidad(LaboresDTO labor);
	
	/**
	 * 
	 * @param labores
	 * @return
	 */
	List<LaboresDTO> laboresEntidadesADTO(List<Labores>labores);
	
	/**
	 * 
	 * @param labores
	 * @return
	 */
	List<Labores> laboresDTOAEntidades(List<LaboresDTO>labores);

}
