/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Labores;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface ILaboresRepo extends CrudRepository<Labores, Long>{

}
