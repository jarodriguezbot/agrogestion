/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_cuenta_contable")
public class CuentaContable {
	
	/**
	 * 
	 */
	public CuentaContable() {
		super();
	}

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cta_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "cta_nombre", length = 255)
	private String ctaNombre;
	
	@Basic(optional = false)
	@Column(name = "cta_cod_debito", length = 10)
	private String ctaCodDebito;
	
	@Basic(optional = false)
	@Column(name = "cta_cod_credito", length = 10)
	private String ctaCodCredito;
	
	@Basic(optional = false)
	@Column(name = "cta_tipo", length = 10)
	private String ctaTipo;
	
	@Basic(optional = false)
	@Column(name = "cta_puc", length = 10)
	private String ctaPuc;
	
	@Basic(optional = false)
	@Column(name = "cta_uso", length = 10)
	private String ctaUso;
	
	@Basic(optional = false)
	@Column(name = "cta_grupo", length = 10)
	private String ctaGrupo;
}
