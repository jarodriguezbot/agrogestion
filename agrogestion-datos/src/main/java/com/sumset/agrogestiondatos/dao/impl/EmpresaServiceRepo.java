/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IEmpresaServiceRepo;
import com.sumset.agrogestiondatos.models.Empresa;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IEmpresaRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class EmpresaServiceRepo 
	extends OperacionesBasicasImpl<Empresa, Long> implements IEmpresaServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IEmpresaRepo empresaRepo;
	
	@Override
	public CrudRepository<Empresa, Long> getRepo() {
		return empresaRepo;
	}


}
