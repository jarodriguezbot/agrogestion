/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.MovimientosContables;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IMovimientosContablesRepo extends CrudRepository<MovimientosContables, Long>{

}
