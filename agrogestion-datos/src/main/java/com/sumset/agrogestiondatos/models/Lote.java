/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_lote")
public class Lote {

	/**
	 * 
	 */
	public Lote() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "lot_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "lot_nombre", length = 255)
	private String lotNombre;

	@Basic(optional = false)
	@Column(name = "lot_codigo", length = 10)
	private String lotCodigo;
	
	@Basic(optional = false)
	@Column(name = "lot_arboles_contados")
	private Long lotArbolesContados;
	
	@Basic(optional = false)
	@Column(name = "lot_area_bruta")
	private Double lotAreaBruta;
	
	@Basic(optional = false)
	@Column(name = "lot_unidad", length = 10)
	private String lotUnidad;
	
	@Basic(optional = false)
	@Column(name = "lot_area_agric")
	private Double lotAreaAgricola;
	
	@Basic(optional = false)
	@Column(name = "lot_cultivo", length = 10)
	private String lotCultivo;
	
	@Basic(optional = false)
	@Column(name = "lot_char")
	private Long lotChar;
	
	@Basic(optional = false)
	@Column(name = "lot_anio_renovacion")
	private Long lotAnioRenovacion;
	
	@Basic(optional = false)
	@Column(name = "lot_estado", length = 255)
	private String lotEstado;
	
	@Basic(optional = false)
	@Column(name = "lot_calculo_x_hectarea")
	private Double lotCalculoHectarea;
	
	@Basic(optional = false)
	@Column(name = "lot_precio_unitario")
	private Double lotPrecioUnitario;
	
	@Basic(optional = false)
	@Column(name = "lot_produccion_proyect")
	private Double lotProducProyectada;
	
	@Basic(optional = false)
	@Column(name = "lot_facturacion")
	private Double lotFacturacion;
	
	@Basic(optional = false)
	@Column(name = "lot_observaciones", length = 10)
	private String lotObservaciones;
	
	@JoinColumn(name = "lot_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centroCosto;
}
