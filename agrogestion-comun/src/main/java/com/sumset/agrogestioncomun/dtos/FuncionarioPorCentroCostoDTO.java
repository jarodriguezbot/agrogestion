/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class FuncionarioPorCentroCostoDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public FuncionarioPorCentroCostoDTO() {
		super();
	}

	private Long id;
	private String fxcEstado;
	private Long fxcCcId;
	private Long fxcFunId;
}
