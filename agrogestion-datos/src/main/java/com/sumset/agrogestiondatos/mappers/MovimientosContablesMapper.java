/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.CuentaContable;
import com.sumset.agrogestiondatos.models.MovimientosContables;

/**
 * @author joals
 *
 */
@Mapper(uses = { CentroDeCosto.class, CuentaContable.class })
public interface MovimientosContablesMapper {

	/**
	 * 
	 * @param movimientoContable
	 * @return
	 */
	@Mapping(source = "movimientoContable.id",  target = "id")
	@Mapping(source = "movimientoContable.mvtFecha",  target = "mvtFecha")
	@Mapping(source = "movimientoContable.mvtOperacion",  target = "mvtOperacion")
	@Mapping(source = "movimientoContable.mvtValorCredito",  target = "mvtValorCredito")
	@Mapping(source = "movimientoContable.mvtValorDebito",  target = "mvtValorDebito")
	@Mapping(source = "movimientoContable.mvtComprobante",  target = "mvtComprobante")
	@Mapping(source = "movimientoContable.cuentaContId.id",  target = "cuentaContId")
	@Mapping(source = "movimientoContable.centoCostoId.id",  target = "centoCostoId")
	MovimientosContablesDTO movimientoContableADTO(MovimientosContables movimientoContable);

	/**
	 * 
	 * @param movimientoContable
	 * @return
	 */
	@Mapping(source = "movimientoContable.id",  target = "id")
	@Mapping(source = "movimientoContable.mvtFecha",  target = "mvtFecha")
	@Mapping(source = "movimientoContable.mvtOperacion",  target = "mvtOperacion")
	@Mapping(source = "movimientoContable.mvtValorCredito",  target = "mvtValorCredito")
	@Mapping(source = "movimientoContable.mvtValorDebito",  target = "mvtValorDebito")
	@Mapping(source = "movimientoContable.mvtComprobante",  target = "mvtComprobante")
	@Mapping(source = "movimientoContable.cuentaContId",  target = "cuentaContId.id")
	@Mapping(source = "movimientoContable.centoCostoId",  target = "centoCostoId.id")
	MovimientosContables movimientoContableDTOAEntidad(MovimientosContablesDTO movimientoContable);

	/**
	 * 
	 * @param movimientosContables
	 * @return
	 */
	List<MovimientosContablesDTO> movimientosContablesEntidadesADTO(List<MovimientosContables> movimientosContables);

	/**
	 * 
	 * @param movimientosContables
	 * @return
	 */
	List<MovimientosContables> movimientosContablesDTOAEntidades(List<MovimientosContablesDTO> movimientosContables);

}
