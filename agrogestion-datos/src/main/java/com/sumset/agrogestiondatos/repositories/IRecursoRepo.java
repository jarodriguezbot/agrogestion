/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Recurso;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IRecursoRepo extends CrudRepository<Recurso, Long>{

}
