/**
 * 
 */
package com.sumset.agrogestiondatos.serviciofacade;

import java.io.Serializable;

import javax.swing.RepaintManager;

import org.springframework.stereotype.Component;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGConstantes;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.models.CuentaContable;
import com.sumset.agrogestiondatos.models.DescuentoNomina;
import com.sumset.agrogestiondatos.models.Empresa;
import com.sumset.agrogestiondatos.models.Funcionario;
import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;
import com.sumset.agrogestiondatos.models.Insumo;
import com.sumset.agrogestiondatos.models.InsumoPorLote;
import com.sumset.agrogestiondatos.models.Labores;
import com.sumset.agrogestiondatos.models.Lote;
import com.sumset.agrogestiondatos.models.MovimientosContables;
import com.sumset.agrogestiondatos.models.Recurso;
import com.sumset.agrogestiondatos.models.Referencia;
import com.sumset.agrogestiondatos.models.Ventas;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Component
public class ServicioFacadeGuardar extends ServicioFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ServicioFacadeGuardar() {
		super();
	}

	/**
	 * Método para agregar o modificar un centro de costo
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO guardarCentroDeCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		CentroDeCostoDTO respuesta = new CentroDeCostoDTO();
		CentroDeCosto centroDeCosto = centroCostoMapper.centroDeCostoDTOAEntidad(centroDeCostoDTO);
		respuesta = centroCostoMapper.centroDeCostoADTO(centroCostoDAO.guardar(centroDeCosto));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Centro de costo guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el centro de costo");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar una compra
	 * 
	 * @param comprasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ComprasDTO guardarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		ComprasDTO respuesta = new ComprasDTO();
		Compras compras = comprasMapper.comprasDTOAEntidad(comprasDTO);
		respuesta = comprasMapper.comprasADTO(comprasDAO.guardar(compras));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Compra guardada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo la compra");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar una cuenta contable
	 * 
	 * @param cuentaContableDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CuentaContableDTO guardarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		CuentaContableDTO respuesta = new CuentaContableDTO();
		CuentaContable cuentaContable = cuentaContableMapper.cuentaContableDTOAEntidad(cuentaContableDTO);
		respuesta = cuentaContableMapper.cuentaContableADTO(cuentaContableDAO.guardar(cuentaContable));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Cuenta contable guardada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo la cuenta contable");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un descuento de nómina
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public DescuentoNominaDTO guardarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		DescuentoNominaDTO respuesta = new DescuentoNominaDTO();
		DescuentoNomina descuentoNomina = descuentoNominaMapper.descuentoNominaDTOAEntidad(descuentoNominaDTO);
		respuesta = descuentoNominaMapper.descuentoNominaADTO(descuentoNominaDAO.guardar(descuentoNomina));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Descuento por nomina guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el descuento por nomina");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar una empresa
	 * 
	 * @param empresaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public EmpresaDTO guardarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		EmpresaDTO respuesta = new EmpresaDTO();
		Empresa empresa = empresaMapper.empresaDTOAEntidad(empresaDTO);
		respuesta = empresaMapper.empresaADTO(empresaDAO.guardar(empresa));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Empresa guardada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo la empresa");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un funcionario por centro de costo
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioPorCentroCostoDTO guardarFuncionarioPorCentroCosto(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		FuncionarioPorCentroCostoDTO respuesta = new FuncionarioPorCentroCostoDTO();
		FuncionarioPorCentroCosto funcionarioPorCentroCosto = funcionarioPorCentroCostoMapper
				.funcionarioCCostoDTOAEntidad(funcionarioPorCentroCostoDTO);
		respuesta = funcionarioPorCentroCostoMapper
				.funcionarioCCostoADTO(funcionarioPorCentroCostoDAO.guardar(funcionarioPorCentroCosto));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionario por centro de costo guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el funcionario por centro de costo");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un funcionario
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioDTO guardarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		FuncionarioDTO respuesta = new FuncionarioDTO();
		Funcionario funcionario = funcionarioMapper.funcionarioDTOAEntidad(funcionarioDTO);
		respuesta = funcionarioMapper.funcionarioADTO(funcionarioDAO.guardar(funcionario));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionario guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el funcionario");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un lote
	 * 
	 * @param insumoPorLoteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoPorLoteDTO guardarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		InsumoPorLoteDTO respuesta = new InsumoPorLoteDTO();
		InsumoPorLote insumoPorLote = insumoPorLoteMapper.insumoPorLoteDTOAEntidad(insumoPorLoteDTO);
		respuesta = insumoPorLoteMapper.insumoPorLoteADTO(insumoPorLoteDAO.guardar(insumoPorLote));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumo por guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el insumo por lote");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un insumo
	 * 
	 * @param insumoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoDTO guardarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		InsumoDTO respuesta = new InsumoDTO();
		Insumo insumo = insumoMapper.insumoDTOAEntidad(insumoDTO);
		respuesta = insumoMapper.insumoADTO(insumoDAO.guardar(insumo));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumo guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el insumo");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar una labor
	 * 
	 * @param laboresDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LaboresDTO guardarLabor(LaboresDTO laboresDTO) throws AGExcepciones {
		LaboresDTO respuesta = new LaboresDTO();
		Labores labores = laboresMapper.laborDTOAEntidad(laboresDTO);
		respuesta = laboresMapper.laborADTO(laboresDAO.guardar(labores));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Labor guardada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo la labor");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un lote
	 * 
	 * @param loteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LoteDTO guardarLote(LoteDTO loteDTO) throws AGExcepciones {
		LoteDTO respuesta = new LoteDTO();
		Lote lote = loteMapper.loteDTOAEntidad(loteDTO);
		respuesta = loteMapper.loteADTO(loteDAO.guardar(lote));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Lote guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el lote");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un movimiento contable
	 * 
	 * @param movimientosContablesDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public MovimientosContablesDTO guardarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones {
		MovimientosContablesDTO respuesta = new MovimientosContablesDTO();
		MovimientosContables movimientosContables = movimientosContablesMapper
				.movimientoContableDTOAEntidad(movimientosContablesDTO);
		respuesta = movimientosContablesMapper
				.movimientoContableADTO(movimientosContablesDAO.guardar(movimientosContables));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Movimiento contable guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el movimiento contable");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar un recurso
	 * 
	 * @param recursoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public RecursoDTO guardarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		RecursoDTO respuesta = new RecursoDTO();
		Recurso recurso = recursoMapper.recursoDTOAEntidad(recursoDTO);
		respuesta = recursoMapper.recursoADTO(recursoDAO.guardar(recurso));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Recurso guardado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo el recurso");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar una referencia
	 * 
	 * @param referenciaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ReferenciaDTO guardarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		ReferenciaDTO respuesta = new ReferenciaDTO();
		Referencia referencia = referenciaMapper.referenciaDTOAEntidad(referenciaDTO);
		respuesta = referenciaMapper.referenciaADTO(referenciaDAO.guardar(referencia));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Referencia guardada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo la referencia");
		}
		return respuesta;
	}

	/**
	 * Método para agregar o modificar una venta
	 * 
	 * @param ventasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public VentasDTO guardarVenta(VentasDTO ventasDTO) throws AGExcepciones {
		VentasDTO respuesta = new VentasDTO();
		Ventas ventas = ventasMapper.ventaDTOAEntidad(ventasDTO);
		respuesta = ventasMapper.ventaADTO(ventasDAO.guardar(ventas));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Venta guardada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se guardo la venta");
		}
		return respuesta;
	}
}
