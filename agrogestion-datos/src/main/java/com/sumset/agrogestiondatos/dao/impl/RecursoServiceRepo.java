/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IRecursoServiceRepo;
import com.sumset.agrogestiondatos.models.Recurso;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IRecursoRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class RecursoServiceRepo 
	extends OperacionesBasicasImpl<Recurso, Long> implements IRecursoServiceRepo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IRecursoRepo recursoRepo;
	
	@Override
	public CrudRepository<Recurso, Long> getRepo() {
		return recursoRepo;
	}


}
