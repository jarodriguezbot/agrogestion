/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Lote;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface ILoteRepo extends CrudRepository<Lote, Long>{

}
