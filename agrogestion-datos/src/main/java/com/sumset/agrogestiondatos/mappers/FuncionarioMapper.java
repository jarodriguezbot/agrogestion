/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.models.Funcionario;

/**
 * @author joals
 *
 */
@Mapper
public interface FuncionarioMapper {
	
	/**
	 * 
	 * @param funcionario
	 * @return
	 */
	@Mapping(source = "funcionario.id",  target = "id")
	@Mapping(source = "funcionario.funCedula",  target = "funCedula")
	@Mapping(source = "funcionario.funNombre",  target = "funNombre")
	@Mapping(source = "funcionario.funApellido",  target = "funApellido")
	@Mapping(source = "funcionario.funCelular",  target = "funCelular")
	@Mapping(source = "funcionario.funCorreo",  target = "funCorreo")
	FuncionarioDTO funcionarioADTO(Funcionario funcionario);
	
	/**
	 * 
	 * @param funcionario
	 * @return
	 */
	@Mapping(source = "funcionario.id",  target = "id")
	@Mapping(source = "funcionario.funCedula",  target = "funCedula")
	@Mapping(source = "funcionario.funNombre",  target = "funNombre")
	@Mapping(source = "funcionario.funApellido",  target = "funApellido")
	@Mapping(source = "funcionario.funCelular",  target = "funCelular")
	@Mapping(source = "funcionario.funCorreo",  target = "funCorreo")
	Funcionario funcionarioDTOAEntidad(FuncionarioDTO funcionario);
	
	/**
	 * 
	 * @param funcionarios
	 * @return
	 */
	List<FuncionarioDTO> funcionarioEntitiesADTO(List<Funcionario> funcionarios);
	
	/**
	 * 
	 * @param funcionarios
	 * @return
	 */
	List<Funcionario> funcionarioDTOAEntities(List<FuncionarioDTO> funcionarios);

}
