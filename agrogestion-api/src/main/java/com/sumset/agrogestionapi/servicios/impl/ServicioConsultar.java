/**
 * 
 */
package com.sumset.agrogestionapi.servicios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sumset.agrogestionapi.servicios.interfaces.IServicioConsultar;
import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.GenericoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGLogger;
import com.sumset.agrogestiondatos.sessionfacade.interfaces.ISessionFacade;

/**
 * @author Sumset
 *
 */
@Service
public class ServicioConsultar implements IServicioConsultar {

	@Autowired
	ISessionFacade sessionFacade;
	
	@Autowired
	AGLogger logger;
	
	/**
	 * 
	 */
	public ServicioConsultar() {
		super();
	}

	@Override
	public GenericoDTO buscarTodosCentroCostos() throws AGExcepciones {
		return sessionFacade.buscarTodosCentroCostos();
	}

	@Override
	public CentroDeCostoDTO buscarCentroCostoPorId(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		return sessionFacade.buscarCentroCostoPorId(centroDeCostoDTO);
	}

	@Override
	public GenericoDTO buscarTodasCompras() throws AGExcepciones {
		return sessionFacade.buscarTodasCompras();
	}

	@Override
	public ComprasDTO buscarCompraPorId(ComprasDTO comprasDTO) throws AGExcepciones {
		return sessionFacade.buscarCompraPorId(comprasDTO);
	}

	@Override
	public GenericoDTO buscarTodasCuentasContables() throws AGExcepciones {
		return sessionFacade.buscarTodasCuentasContables();
	}

	@Override
	public CuentaContableDTO buscarCuentaContablePorId(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		return sessionFacade.buscarCuentaContablePorId(cuentaContableDTO);
	}

	@Override
	public GenericoDTO buscarTodosDescuentosNomina() throws AGExcepciones {
		return sessionFacade.buscarTodosDescuentosNomina();
	}

	@Override
	public DescuentoNominaDTO buscarDescuentoNominaPorId(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		return sessionFacade.buscarDescuentoNominaPorId(descuentoNominaDTO);
	}

	@Override
	public GenericoDTO buscarTodasEmpresas() throws AGExcepciones {
		return sessionFacade.buscarTodasEmpresas();
	}

	@Override
	public EmpresaDTO buscarEmpresaPorId(EmpresaDTO empresaDTO) throws AGExcepciones {
		return sessionFacade.buscarEmpresaPorId(empresaDTO);
	}

	@Override
	public GenericoDTO buscarTodosFuncionariosPorCentroCosto() throws AGExcepciones {
		return sessionFacade.buscarTodosFuncionariosPorCentroCosto();
	}

	@Override
	public FuncionarioPorCentroCostoDTO buscarFuncionarioPorCentroCostoPorId(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		return sessionFacade.buscarFuncionarioPorCentroCostoPorId(funcionarioPorCentroCostoDTO);
	}

	@Override
	public GenericoDTO buscarTodosFuncionarios() throws AGExcepciones {
		return sessionFacade.buscarTodosFuncionarios();
	}

	@Override
	public FuncionarioDTO buscarFuncionarioPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		return sessionFacade.buscarFuncionarioPorId(funcionarioDTO);
	}

	@Override
	public GenericoDTO buscarTodosInsumosPorLote() throws AGExcepciones {
		return sessionFacade.buscarTodosInsumosPorLote();
	}

	@Override
	public InsumoPorLoteDTO buscarInsumosPorLotePorId(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		return sessionFacade.buscarInsumosPorLotePorId(insumoPorLoteDTO);
	}

	@Override
	public GenericoDTO buscarTodosInsumos() throws AGExcepciones {
		return sessionFacade.buscarTodosInsumos();
	}

	@Override
	public InsumoDTO buscarInsumosPorId(InsumoDTO insumoDTO) throws AGExcepciones {
		return sessionFacade.buscarInsumosPorId(insumoDTO);
	}

	@Override
	public GenericoDTO buscarTodasLabores() throws AGExcepciones {
		return sessionFacade.buscarTodasLabores();
	}

	@Override
	public LaboresDTO buscarLaboresPorId(LaboresDTO laboresDTO) throws AGExcepciones {
		return sessionFacade.buscarLaboresPorId(laboresDTO);
	}

	@Override
	public GenericoDTO buscarTodosLotes() throws AGExcepciones {
		return sessionFacade.buscarTodosLotes();
	}

	@Override
	public LoteDTO buscarLotePorId(LoteDTO loteDTO) throws AGExcepciones {
		return sessionFacade.buscarLotePorId(loteDTO);
	}

	@Override
	public GenericoDTO buscarTodosMovimientosContables() throws AGExcepciones {
		return sessionFacade.buscarTodosMovimientosContables();
	}

	@Override
	public MovimientosContablesDTO buscarMovimientosContablesPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		return sessionFacade.buscarMovimientosContablesPorId(funcionarioDTO);
	}

	@Override
	public GenericoDTO buscarTodosRecursos() throws AGExcepciones {
		return sessionFacade.buscarTodosRecursos();
	}

	@Override
	public RecursoDTO buscarRecursoPorId(RecursoDTO recursoDTO) throws AGExcepciones {
		return sessionFacade.buscarRecursoPorId(recursoDTO);
	}

	@Override
	public GenericoDTO buscarTodasReferencias() throws AGExcepciones {
		return sessionFacade.buscarTodasReferencias();
	}

	@Override
	public ReferenciaDTO buscarReferenciaPorId(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		return sessionFacade.buscarReferenciaPorId(referenciaDTO);
	}

	@Override
	public GenericoDTO buscarTodasVentas() throws AGExcepciones {
		return sessionFacade.buscarTodasVentas();
	}

	@Override
	public VentasDTO buscarVentasPorId(VentasDTO ventasDTO) throws AGExcepciones {
		return sessionFacade.buscarVentasPorId(ventasDTO);
	}

}
