package com.sumset.agrogestiondatos.dao.interfaces;

import java.io.Serializable;

import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.operacionesbasicas.interfaces.IOperacionesBasicas;

public interface ICentroDeCostoServiceRepo extends IOperacionesBasicas<CentroDeCosto, Long>, Serializable {

}
