/**
 * 
 */
package com.sumset.agrogestionapi.servicios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sumset.agrogestionapi.servicios.interfaces.IServicioGuardar;
import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGLogger;
import com.sumset.agrogestiondatos.sessionfacade.interfaces.ISessionFacade;

/**
 * @author Sumset
 *
 */
@Service
public class ServicioGuardar implements IServicioGuardar {

	@Autowired
	ISessionFacade sessionFacade;
	
	@Autowired
	AGLogger logger;
	
	/**
	 * 
	 */
	public ServicioGuardar() {
		super();
	}

	@Override
	public CentroDeCostoDTO guardarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		return sessionFacade.guardarCentroCosto(centroDeCostoDTO);
	}

	@Override
	public CentroDeCostoDTO modificarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		return sessionFacade.modificarCentroCosto(centroDeCostoDTO);
	}

	@Override
	public ComprasDTO guardarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		return sessionFacade.guardarCompra(comprasDTO);
	}

	@Override
	public CuentaContableDTO guardarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		return sessionFacade.guardarCuentaContable(cuentaContableDTO);
	}

	@Override
	public DescuentoNominaDTO guardarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		return sessionFacade.guardarDescuentoNomina(descuentoNominaDTO);
	}

	@Override
	public EmpresaDTO guardarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		return sessionFacade.guardarEmpresa(empresaDTO);
	}

	@Override
	public FuncionarioPorCentroCostoDTO guardarFuncionarioPorCentroCosto(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		
		return sessionFacade.guardarFuncionarioPorCentroCosto(funcionarioPorCentroCostoDTO);
	}

	@Override
	public FuncionarioDTO guardarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		return sessionFacade.guardarFuncionario(funcionarioDTO);
	}

	@Override
	public InsumoPorLoteDTO guardarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		return sessionFacade.guardarInsumoPorLote(insumoPorLoteDTO);
	}

	@Override
	public InsumoDTO guardarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		return sessionFacade.guardarInsumo(insumoDTO);
	}

	@Override
	public LaboresDTO guardarLabor(LaboresDTO laboresDTO) throws AGExcepciones {
		return sessionFacade.guardarLabor(laboresDTO);
	}

	@Override
	public LoteDTO guardarLote(LoteDTO loteDTO) throws AGExcepciones {
		return sessionFacade.guardarLote(loteDTO);
	}

	@Override
	public MovimientosContablesDTO guardarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones {

		return sessionFacade.guardarMovimientoContable(movimientosContablesDTO);
	}

	@Override
	public RecursoDTO guardarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		return sessionFacade.guardarRecurso(recursoDTO);
	}

	@Override
	public ReferenciaDTO guardarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		return sessionFacade.guardarReferencia(referenciaDTO);
	}

	@Override
	public VentasDTO guardarVenta(VentasDTO ventasDTO) throws AGExcepciones {
		return sessionFacade.guardarVenta(ventasDTO);
	}

}
