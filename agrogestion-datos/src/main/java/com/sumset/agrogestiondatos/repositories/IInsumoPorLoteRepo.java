package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.InsumoPorLote;

public interface IInsumoPorLoteRepo extends CrudRepository<InsumoPorLote, Long>{

}
