/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.sql.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class DescuentoNominaDTO extends RespuestaDTO{

	/**
	 * 
	 */
	public DescuentoNominaDTO() {
		super();
	}

	private Long id;
	private Date dnoFecha;
	private Long dnoCorte;
	private String dnoTipoDescuento;
	private String dnoNombreFuncionario;
	private String dnoCedulaFuncionario;
	private Long dnoCantidad;
	private Double dnoPrecio;
	private Double dnoValorTotal;
	private String dnoDebito;
	private String dnoComprobante;
	private Long dnoNumeroComprobante;
	private Long dnoFuncId;
	private Long dnoLoteId;
}
