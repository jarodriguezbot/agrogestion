/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.models.Funcionario;
import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;

/**
 * @author joals
 *
 */
@Mapper(uses = {CentroDeCosto.class, Funcionario.class})
public interface FuncionarioPorCentroDeCostoMapper {

	/**
	 * 
	 * @param funcionarioPorCentroCosto
	 * @return
	 */
	@Mapping(source = "funcionarioPorCentroCosto.id",  target = "id")
	@Mapping(source = "funcionarioPorCentroCosto.fxcEstado",  target = "fxcEstado")
	@Mapping(source = "funcionarioPorCentroCosto.centroCosto.id",  target = "fxcCcId")
	@Mapping(source = "funcionarioPorCentroCosto.fxcFunId.id",  target = "fxcFunId")
	FuncionarioPorCentroCostoDTO funcionarioCCostoADTO(FuncionarioPorCentroCosto funcionarioPorCentroCosto);

	/**
	 * 
	 * @param funcionarioPorCentroCosto
	 * @return
	 */
	@Mapping(source = "funcionarioPorCentroCosto.id",  target = "id")
	@Mapping(source = "funcionarioPorCentroCosto.fxcEstado",  target = "fxcEstado")
	@Mapping(source = "funcionarioPorCentroCosto.fxcCcId",  target = "centroCosto.id")
	@Mapping(source = "funcionarioPorCentroCosto.fxcFunId",  target = "fxcFunId.id")
	FuncionarioPorCentroCosto funcionarioCCostoDTOAEntidad(FuncionarioPorCentroCostoDTO funcionarioPorCentroCosto);

	/**
	 * 
	 * @param funcionarioPorCentroCostos
	 * @return
	 */
	List<FuncionarioPorCentroCostoDTO> funcionarioCCostoEntitiesADTO(
			List<FuncionarioPorCentroCosto> funcionarioPorCentroCostos);

	/**
	 * 
	 * @param funcionarioPorCentroCostos
	 * @return
	 */
	List<FuncionarioPorCentroCosto> funcionarioCCostoDTOAEntities(
			List<FuncionarioPorCentroCostoDTO> funcionarioPorCentroCostos);
}
