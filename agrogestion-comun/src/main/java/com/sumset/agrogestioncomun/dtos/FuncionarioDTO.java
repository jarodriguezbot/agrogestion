/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class FuncionarioDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public FuncionarioDTO() {
		super();
	}

	private Long id;
	private String funCedula;
	private String funNombre;
	private String funApellido;
	private String funCelular;
	private String funCorreo;
	
}
