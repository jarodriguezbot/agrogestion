/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.CentroDeCosto;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface ICentroDeCostoRepo extends CrudRepository<CentroDeCosto, Long>{

}
