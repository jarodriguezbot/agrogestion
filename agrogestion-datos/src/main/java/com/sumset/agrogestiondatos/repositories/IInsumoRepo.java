/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Insumo;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IInsumoRepo extends CrudRepository<Insumo, Long>{

}
