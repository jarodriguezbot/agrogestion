/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Ventas;

/**
 * @author joals
 *
 */
@Mapper(uses = {CentroDeCosto.class})
public interface VentasMapper {
	
	/**
	 * 
	 * @param venta
	 * @return
	 */
	@Mapping(source = "venta.id",  target = "id")
	@Mapping(source = "venta.vtaComprobante",  target = "vtaComprobante")
	@Mapping(source = "venta.vtaFecha",  target = "vtaFecha")
	@Mapping(source = "venta.vtaCorte",  target = "vtaCorte")
	@Mapping(source = "venta.vtaTercero",  target = "vtaTercero")
	@Mapping(source = "venta.vtaCredito",  target = "vtaCredito")
	@Mapping(source = "venta.vtaCantidad",  target = "vtaCantidad")
	@Mapping(source = "venta.vtaUnidad",  target = "vtaUnidad")
	@Mapping(source = "venta.vtaPrecio",  target = "vtaPrecio")
	@Mapping(source = "venta.vtaValorTotal",  target = "vtaValorTotal")
	@Mapping(source = "venta.vtaCodDebito",  target = "vtaCodDebito")
	@Mapping(source = "venta.vtaEstadoCultivo",  target = "vtaEstadoCultivo")
	@Mapping(source = "venta.centroCosto.id",  target = "vtaCcId")
	VentasDTO ventaADTO(Ventas venta);
	
	/**
	 * 
	 * @param venta
	 * @return
	 */
	@Mapping(source = "venta.id",  target = "id")
	@Mapping(source = "venta.vtaComprobante",  target = "vtaComprobante")
	@Mapping(source = "venta.vtaFecha",  target = "vtaFecha")
	@Mapping(source = "venta.vtaCorte",  target = "vtaCorte")
	@Mapping(source = "venta.vtaTercero",  target = "vtaTercero")
	@Mapping(source = "venta.vtaCredito",  target = "vtaCredito")
	@Mapping(source = "venta.vtaCantidad",  target = "vtaCantidad")
	@Mapping(source = "venta.vtaUnidad",  target = "vtaUnidad")
	@Mapping(source = "venta.vtaPrecio",  target = "vtaPrecio")
	@Mapping(source = "venta.vtaValorTotal",  target = "vtaValorTotal")
	@Mapping(source = "venta.vtaCodDebito",  target = "vtaCodDebito")
	@Mapping(source = "venta.vtaEstadoCultivo",  target = "vtaEstadoCultivo")
	@Mapping(source = "venta.vtaCcId",  target = "centroCosto.id")
	Ventas ventaDTOAEntidad(VentasDTO venta);
	
	/**
	 * 
	 * @param ventas
	 * @return
	 */
	List<VentasDTO> ventasEntidadesADTO(List<Ventas> ventas);
	
	/**
	 * 
	 * @param ventas
	 * @return
	 */
	List<Ventas> ventasDTOAEntidades(List<VentasDTO> ventas);

}
