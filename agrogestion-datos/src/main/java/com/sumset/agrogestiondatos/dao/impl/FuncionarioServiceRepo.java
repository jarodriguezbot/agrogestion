/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IFuncionarioServiceRepo;
import com.sumset.agrogestiondatos.models.Funcionario;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IFuncionarioRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class FuncionarioServiceRepo 
	extends OperacionesBasicasImpl<Funcionario, Long> implements IFuncionarioServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IFuncionarioRepo funcionarioRepo;
	
	@Override
	public CrudRepository<Funcionario, Long> getRepo() {
		return funcionarioRepo;
	}

}
