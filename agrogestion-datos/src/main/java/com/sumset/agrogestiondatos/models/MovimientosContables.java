/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_mvto_contab")
public class MovimientosContables {

	/**
	 * 
	 */
	public MovimientosContables() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mvt_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "mvt_fecha")
	private Date mvtFecha;
	
	@Basic(optional = false)
	@Column(name = "mvt_operacion", length = 10)
	private String mvtOperacion;
	
	@Basic(optional = false)
	@Column(name = "mvt_vlr_credito")
	private Double mvtValorCredito;
	
	@Basic(optional = false)
	@Column(name = "mvt_vlr_debito")
	private Double mvtValorDebito;
	
	@Basic(optional = false)
	@Column(name = "mvt_comprobante", length = 255)
	private String mvtComprobante;

	@JoinColumn(name = "mvt_cta_id", referencedColumnName = "cta_id")
	@ManyToOne
	private CuentaContable cuentaContId;
	
	@JoinColumn(name = "mvt_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centoCostoId;
}
