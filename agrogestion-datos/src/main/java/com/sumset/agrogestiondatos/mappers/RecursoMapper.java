/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Recurso;

/**
 * @author joals
 *
 */
@Mapper(uses = {CentroDeCosto.class})
public interface RecursoMapper {
	
	/**
	 * 
	 * @param recurso
	 * @return
	 */
	@Mapping(source = "recurso.id",  target = "id")
	@Mapping(source = "recurso.recNombre",  target = "recNombre")
	@Mapping(source = "recurso.recDescripcion",  target = "recDescripcion")
	@Mapping(source = "recurso.recUnidad",  target = "recUnidad")
	@Mapping(source = "recurso.recPrecio",  target = "recPrecio")
	@Mapping(source = "recurso.centroCosto.id",  target = "recCcId")
	RecursoDTO recursoADTO(Recurso recurso);
	
	/**
	 * 
	 * @param recurso
	 * @return
	 */
	@Mapping(source = "recurso.id",  target = "id")
	@Mapping(source = "recurso.recNombre",  target = "recNombre")
	@Mapping(source = "recurso.recDescripcion",  target = "recDescripcion")
	@Mapping(source = "recurso.recUnidad",  target = "recUnidad")
	@Mapping(source = "recurso.recPrecio",  target = "recPrecio")
	@Mapping(source = "recurso.recCcId",  target = "centroCosto.id")
	Recurso recursoDTOAEntidad(RecursoDTO recurso);
	
	/**
	 * 
	 * @param recursos
	 * @return
	 */
	List<RecursoDTO> recursosEntidadesADTO(List<Recurso>recursos);
	
	/**
	 * 
	 * @param recursos
	 * @return
	 */
	List<Recurso> recursosDTOAEntidades(List<RecursoDTO>recursos);
}
