/**
 * 
 */
package com.sumset.agrogestiondatos.serviciofacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.GenericoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGConstantes;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.models.CuentaContable;
import com.sumset.agrogestiondatos.models.DescuentoNomina;
import com.sumset.agrogestiondatos.models.Empresa;
import com.sumset.agrogestiondatos.models.Funcionario;
import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;
import com.sumset.agrogestiondatos.models.Insumo;
import com.sumset.agrogestiondatos.models.InsumoPorLote;
import com.sumset.agrogestiondatos.models.Labores;
import com.sumset.agrogestiondatos.models.Lote;
import com.sumset.agrogestiondatos.models.MovimientosContables;
import com.sumset.agrogestiondatos.models.Recurso;
import com.sumset.agrogestiondatos.models.Referencia;
import com.sumset.agrogestiondatos.models.Ventas;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Component
public class ServicioFacadeConsulta extends ServicioFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ServicioFacadeConsulta() {
		super();
	}

	/**
	 * Método que retorna una lista con todo los centros de costo
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosCentroCostos() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<CentroDeCostoDTO> lstCentroCostoDTO = centroCostoMapper
				.centroDeCostosEntidadesADTO(centroCostoDAO.obtenerTodos());
		if (lstCentroCostoDTO != null && lstCentroCostoDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstCentroCostoDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Todos los Centros de Costo consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar un centro de costo por Id
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO buscarCentroCostoPorId(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		CentroDeCostoDTO respuesta = centroCostoMapper
				.centroDeCostoADTO(centroCostoDAO.obtener(centroDeCostoDTO.getId()));
		if (respuesta != null && respuesta.getId() > 0) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Centro de Costo consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todas las compras
	 * 
	 * @return retorna una lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasCompras() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<ComprasDTO> lstComprasDTO = comprasMapper.comprasEntidadesADTO(comprasDAO.obtenerTodos());
		if (lstComprasDTO != null && lstComprasDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstComprasDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Todas las compras consultadas exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar compras por Id
	 * 
	 * @param comprasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ComprasDTO buscarCompraPorId(ComprasDTO comprasDTO) throws AGExcepciones {
		ComprasDTO respuesta = comprasMapper.comprasADTO(comprasDAO.obtener(comprasDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Empresa consultada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todas las cuentas contables
	 * 
	 * @return lista con todas las cuentas GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasCuentasContables() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<CuentaContableDTO> lstCuentaContableDTO = cuentaContableMapper
				.cuentaContableEntidadesADTO(cuentaContableDAO.obtenerTodos());
		if (lstCuentaContableDTO != null && lstCuentaContableDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstCuentaContableDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Todas las cuentas contables consultadas exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar cuenta contable por Id
	 * 
	 * @param cuentaContableDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CuentaContableDTO buscarCuentaContablePorId(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		CuentaContableDTO respuesta = cuentaContableMapper
				.cuentaContableADTO(cuentaContableDAO.obtener(cuentaContableDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Cuenta contable consultada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los descuentos de nómina
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosDescuentosNomina() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<DescuentoNominaDTO> lstDescuentoNominaDTO = descuentoNominaMapper
				.descuentosNominaEntidadesADTO(descuentoNominaDAO.obtenerTodos());
		if (lstDescuentoNominaDTO != null && lstDescuentoNominaDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstDescuentoNominaDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Descuentos por nomina consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar un descuento de nómina por Id
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public DescuentoNominaDTO buscarDescuentoNominaPorId(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		DescuentoNominaDTO respuesta = descuentoNominaMapper
				.descuentoNominaADTO(descuentoNominaDAO.obtener(descuentoNominaDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Descuento por nomina consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todas las empresas
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasEmpresas() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<EmpresaDTO> lstEmpresaDTO = empresaMapper.empresasEntitiesADTO(empresaDAO.obtenerTodos());
		if (lstEmpresaDTO != null && lstEmpresaDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstEmpresaDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Empresas consultadas exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar empresas por Id
	 * 
	 * @param empresaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public EmpresaDTO buscarEmpresaPorId(EmpresaDTO empresaDTO) throws AGExcepciones {
		EmpresaDTO respuesta = empresaMapper.empresaADTO(empresaDAO.obtener(empresaDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Empresa consultada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los funcionarios por Centro de Costo
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosFuncionariosPorCentroCosto() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<FuncionarioPorCentroCostoDTO> lstFuncionarioPorCentroCostoDTO = funcionarioPorCentroCostoMapper
				.funcionarioCCostoEntitiesADTO(funcionarioPorCentroCostoDAO.obtenerTodos());
		if (lstFuncionarioPorCentroCostoDTO != null && lstFuncionarioPorCentroCostoDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstFuncionarioPorCentroCostoDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionarios por centro de costo consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar un funcionario por centro de costo por Id
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioPorCentroCostoDTO buscarFuncionarioPorCentroCostoPorId(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		FuncionarioPorCentroCostoDTO respuesta = funcionarioPorCentroCostoMapper
				.funcionarioCCostoADTO(funcionarioPorCentroCostoDAO.obtener(funcionarioPorCentroCostoDTO.getId()));

		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionario por centro de costo consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los funcionarios
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosFuncionarios() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<FuncionarioDTO> lstFuncionarioDTO = funcionarioMapper
				.funcionarioEntitiesADTO(funcionarioDAO.obtenerTodos());
		if (lstFuncionarioDTO != null && lstFuncionarioDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstFuncionarioDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionarios consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar un funcionario por Id
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioDTO buscarFuncionarioPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		FuncionarioDTO respuesta = funcionarioMapper.funcionarioADTO(funcionarioDAO.obtener(funcionarioDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionario consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los insumos por lote
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosInsumosPorLote() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<InsumoPorLoteDTO> lstInsumoPorLoteDTO = insumoPorLoteMapper
				.insumosPorLotesEntidadesADTO(insumoPorLoteDAO.obtenerTodos());
		if (lstInsumoPorLoteDTO != null && lstInsumoPorLoteDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstInsumoPorLoteDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumos por lote consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar los insumos por lote por Id
	 * 
	 * @param insumoPorLoteDTO
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public InsumoPorLoteDTO buscarInsumosPorLotePorId(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		InsumoPorLoteDTO respuesta = insumoPorLoteMapper
				.insumoPorLoteADTO(insumoPorLoteDAO.obtener(insumoPorLoteDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumo por lote consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los insumos
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosInsumos() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<InsumoDTO> lstInsumoDTO = insumoMapper.insumosEntidadesADTO(insumoDAO.obtenerTodos());
		if (lstInsumoDTO != null && lstInsumoDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstInsumoDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumos consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar insumo por Id
	 * 
	 * @param insumoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoDTO buscarInsumosPorId(InsumoDTO insumoDTO) throws AGExcepciones {
		InsumoDTO respuesta = insumoMapper.insumoADTO(insumoDAO.obtener(insumoDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumo consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todas las labores
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasLabores() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<LaboresDTO> lstLaboresDTO = laboresMapper.laboresEntidadesADTO(laboresDAO.obtenerTodos());
		if (lstLaboresDTO != null && lstLaboresDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstLaboresDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Labores consultadas exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar labores por Id
	 * 
	 * @param laboresDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LaboresDTO buscarLaboresPorId(LaboresDTO laboresDTO) throws AGExcepciones {
		LaboresDTO respuesta = laboresMapper.laborADTO(laboresDAO.obtener(laboresDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Labor consultada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los lotes
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosLotes() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<LoteDTO> lstLoteDTO = loteMapper.lotesEntidadesADTO(loteDAO.obtenerTodos());
		if (lstLoteDTO != null && lstLoteDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstLoteDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Lotes consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar lote por Id
	 * 
	 * @param loteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LoteDTO buscarLotePorId(LoteDTO loteDTO) throws AGExcepciones {
		LoteDTO respuesta = loteMapper.loteADTO(loteDAO.obtener(loteDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Lote consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los movimientos contables
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosMovimientosContables() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<MovimientosContablesDTO> lstMovimientosContablesDTO = movimientosContablesMapper
				.movimientosContablesEntidadesADTO(movimientosContablesDAO.obtenerTodos());
		if (lstMovimientosContablesDTO != null && lstMovimientosContablesDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstMovimientosContablesDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Movimientos contables consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar un movimiento contable por Id
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public MovimientosContablesDTO buscarMovimientosContablesPorId(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones {
		MovimientosContablesDTO respuesta = movimientosContablesMapper
				.movimientoContableADTO(movimientosContablesDAO.obtener(movimientosContablesDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Movimiento contable consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todos los recursos
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosRecursos() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<RecursoDTO> lstRecursoDTO = recursoMapper.recursosEntidadesADTO(recursoDAO.obtenerTodos());
		if (lstRecursoDTO != null && lstRecursoDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstRecursoDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Recursos consultados exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;

	}

	/**
	 * Método para buscar un recurso por Id
	 * 
	 * @param recursoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public RecursoDTO buscarRecursoPorId(RecursoDTO recursoDTO) throws AGExcepciones {
		RecursoDTO respuesta = recursoMapper.recursoADTO(recursoDAO.obtener(recursoDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Recurso consultado exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todas la referencias
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasReferencias() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<ReferenciaDTO> lstReferenciaDTO = referenciaMapper.referenciasEntidadesADTO(referenciaDAO.obtenerTodos());
		if (lstReferenciaDTO != null && lstReferenciaDTO.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstReferenciaDTO));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Referencias consultadas exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar referencia por Id
	 * 
	 * @param referenciaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ReferenciaDTO buscarReferenciaPorId(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		ReferenciaDTO respuesta = referenciaMapper.referenciaADTO(referenciaDAO.obtener(referenciaDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Referencia consultada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}

	/**
	 * Método para buscar todas las ventas
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasVentas() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		List<VentasDTO> lstVentasDTOs = ventasMapper.ventasEntidadesADTO(ventasDAO.obtenerTodos());
		if (lstVentasDTOs != null && lstVentasDTOs.size() > 0) {
			respuesta.setListaObjetos(new ArrayList<>(lstVentasDTOs));
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Ventas consultadas exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No hay se encontraron registros para esta consulta");
		}
		return respuesta;
	}

	/**
	 * Método para buscar una venta por Id
	 * 
	 * @param ventasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public VentasDTO buscarVentasPorId(VentasDTO ventasDTO) throws AGExcepciones {
		VentasDTO respuesta = ventasMapper.ventaADTO(ventasDAO.obtener(ventasDTO.getId()));
		if (respuesta != null && respuesta.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Venta consultada exitosamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("No se encontraron registros con este ID");
		}
		return respuesta;
	}
}
