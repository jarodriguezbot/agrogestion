/**
 * 
 */
package com.sumset.agrogestionapi.servicios.interfaces;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.GenericoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;

/**
 * @author Sumset
 *
 */
public interface IServicioConsultar {

	/**
	 * Método que nos permite consultar todos los centros de costo
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosCentroCostos() throws AGExcepciones;

	/**
	 * Método que retorna un centro de costo por Id
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO buscarCentroCostoPorId(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;


	/**
	 * Método para buscar todas las compras
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasCompras() throws AGExcepciones;

	/**
	 * Método para buscar compras por Id
	 * 
	 * @param comprasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ComprasDTO buscarCompraPorId(ComprasDTO comprasDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las cuentas contables
	 * 
	 * @return lista con todas las cuentas GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasCuentasContables() throws AGExcepciones;

	/**
	 * Método para buscar cuenta contable por Id
	 * 
	 * @param cuentaContableDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CuentaContableDTO buscarCuentaContablePorId(CuentaContableDTO cuentaContableDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los descuentos de nómina
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosDescuentosNomina() throws AGExcepciones;

	/**
	 * Método para buscar un descuento de nómina por Id
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public DescuentoNominaDTO buscarDescuentoNominaPorId(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las empresas
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasEmpresas() throws AGExcepciones;

	/**
	 * Método para buscar empresas por Id
	 * 
	 * @param empresaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public EmpresaDTO buscarEmpresaPorId(EmpresaDTO empresaDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los funcionarios por Centro de Costo
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosFuncionariosPorCentroCosto() throws AGExcepciones;

	/**
	 * Método para buscar un funcionario por centro de costo por Id
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioPorCentroCostoDTO buscarFuncionarioPorCentroCostoPorId(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los funcionarios
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosFuncionarios() throws AGExcepciones;

	/**
	 * Método para buscar un funcionario por Id
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioDTO buscarFuncionarioPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los insumos por lote
	 * 
	 * @return
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosInsumosPorLote() throws AGExcepciones;

	/**
	 * Método para buscar los insumos por lote por Id
	 * 
	 * @param insumoPorLoteDTO
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public InsumoPorLoteDTO buscarInsumosPorLotePorId(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los insumos
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosInsumos() throws AGExcepciones;

	/**
	 * Método para buscar insumo por Id
	 * 
	 * @param insumoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoDTO buscarInsumosPorId(InsumoDTO insumoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las labores
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasLabores() throws AGExcepciones;

	/**
	 * Método para buscar labores por Id
	 * 
	 * @param laboresDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LaboresDTO buscarLaboresPorId(LaboresDTO laboresDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los lotes
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosLotes() throws AGExcepciones;

	/**
	 * Método para buscar lote por Id
	 * 
	 * @param loteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LoteDTO buscarLotePorId(LoteDTO loteDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los movimientos contables
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosMovimientosContables() throws AGExcepciones;

	/**
	 * Método para buscar un movimiento contable por Id
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public MovimientosContablesDTO buscarMovimientosContablesPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para buscar todos los recursos
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodosRecursos() throws AGExcepciones;

	/**
	 * Método para buscar un recurso por Id
	 * 
	 * @param recursoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public RecursoDTO buscarRecursoPorId(RecursoDTO recursoDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas la referencias
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasReferencias() throws AGExcepciones;

	/**
	 * Método para buscar referencia por Id
	 * 
	 * @param referenciaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ReferenciaDTO buscarReferenciaPorId(ReferenciaDTO referenciaDTO) throws AGExcepciones;

	/**
	 * Método para buscar todas las ventas
	 * 
	 * @return lista GenericoDTO
	 * @throws AGExcepciones
	 */
	public GenericoDTO buscarTodasVentas() throws AGExcepciones;

	/**
	 * Método para buscar una venta por Id
	 * 
	 * @param ventasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public VentasDTO buscarVentasPorId(VentasDTO ventasDTO) throws AGExcepciones;

}
