package com.sumset.agrogestionapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgrogestionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgrogestionApiApplication.class, args);
	}

}
