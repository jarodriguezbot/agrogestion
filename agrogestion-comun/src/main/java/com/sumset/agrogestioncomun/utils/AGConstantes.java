/**
 * 
 */
package com.sumset.agrogestioncomun.utils;

/**
 * @author Agustín Palomino Pardo
 *
 */
public class AGConstantes {
	/**
	 * Seguridad Token
	 */
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String AUTHORITIES_KEY = "scopes";

	public static final String JWT_SECRETO = "#S4b4n4B0g.";
	public static final int JWT_EXPIRACION_MS_SESSION = 180000000;

	/**
	 * Log severity
	 */
	public static final String SEVERIDAD_DEBUG = "debug";
	public static final String SEVERIDAD_ERROR = "error";
	public static final String SEVERIDAD_FATAL = "fatal";
	public static final String SEVERIDAD_INFO = "info";
	public static final String SEVERIDAD_TRACE = "trace";
	public static final String SEVERIDAD_WARN = "warn";

	/**
	 * Formatos
	 */
	public static final String FORMAT_DATETIME_SHORT_WITH_DASH = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_DATE_SHORT_WITH_DASH = "yyyy-MM-dd";
	public static final String FORMAT_DATETIME_SHORT_WITH_SLASH = "yyyy/MM/dd HH:mm:ss";
	public static final String FORMAT_DATE_SHORT_WITH_SLASH = "yyyy/MM/dd";
	public static final String FORMAT_DATE_CREDITCARD_WITH_SLASH = "yyyy/MM";
	public static final String FORMAT_TIME = "HH:mm:ss";
	public static final String FORMAT_SHORT_TIME = "HH:mm";

	/**
	 * Código de Mensajes
	 */
	public static final int CODIGO_RESPUESTA_EXITOSA = 200;
	public static final int CODIGO_RESPUESTA_ERROR = 400;
	public static final int CODIGO_RESPUESTA_EXCEPTION = 500;

	/**
	 * Descripcion Mensajes
	 */
	public static final String CENCOS_MSG_SUC_BUSCAR_TODOS = "cencos_msg_suc_buscar_todos";

	/**
	 * Error messages
	 */
	public static final String MESSAGE_NOT_FOUND = "Mensaje no encontrado";

	public static final String WEB_CONTROLLER_GET_HOME = "/home";
	public static final String WEB_CONTROLLER_REQUEST_INDEX = "/";

	// Centro de Costo
	public static final String CENCOS_CONTROLLER_BUSCAR_TODOS = "/cencos/buscartodos";
	public static final String CENCOS_CONTROLLER_BUSCAR_POR_ID = "/cencos/buscarporid";
	public static final String CENCOS_CONTROLLER_CREAR = "/cencos/crear";
	public static final String CENCOS_CONTROLLER_MODIFICAR = "/cencos/modificar";
	public static final String CENCOS_CONTROLLER_ELIMINAR = "/cencos/eliminar";

	// Compras
	public static final String COMPRAS_CONTROLLER_BUSCAR_TODOS = "/compras/buscartodos";
	public static final String COMPRAS_CONTROLLER_BUSCAR_POR_ID = "/compras/buscarporid";
	public static final String COMPRAS_CONTROLLER_CREAR = "/compras/crear";
	public static final String COMPRAS_CONTROLLER_ELIMINAR = "/compras/eliminar";

	// Cuenta Contable
	public static final String CUCONT_CONTROLLER_BUSCAR_TODOS = "/cucont/buscartodos";
	public static final String CUCONT_CONTROLLER_BUSCAR_POR_ID = "/cucont/buscarporid";
	public static final String CUCONT_CONTROLLER_CREAR = "/cucont/crear";
	public static final String CUCONT_CONTROLLER_ELIMINAR = "/cucont/eliminar";

	// Descuento de Nómina
	public static final String DESNOM_CONTROLLER_BUSCAR_TODOS = "/desnom/buscartodos";
	public static final String DESNOM_CONTROLLER_BUSCAR_POR_ID = "/desnom/buscarporid";
	public static final String DESNOM_CONTROLLER_CREAR = "/desnom/crear";
	public static final String DESNOM_CONTROLLER_ELIMINAR = "/desnom/eliminar";

	// Empresa
	public static final String EMPRES_CONTROLLER_BUSCAR_TODOS = "/empres/buscartodos";
	public static final String EMPRES_CONTROLLER_BUSCAR_POR_ID = "/empres/buscarporid";
	public static final String EMPRES_CONTROLLER_CREAR = "/empres/crear";
	public static final String EMPRES_CONTROLLER_ELIMINAR = "/empres/eliminar";

	// Funcionario
	public static final String FUNCIO_CONTROLLER_BUSCAR_TODOS = "/funcio/buscartodos";
	public static final String FUNCIO_CONTROLLER_BUSCAR_POR_ID = "/funcio/buscarporid";
	public static final String FUNCIO_CONTROLLER_CREAR = "/funcio/crear";
	public static final String FUNCIO_CONTROLLER_ELIMINAR = "/funcio/eliminar";

	// Funcionario por Centro de Costo
	public static final String FUNCC_CONTROLLER_BUSCAR_TODOS = "/funcc/buscartodos";
	public static final String FUNCC_CONTROLLER_BUSCAR_POR_ID = "/funcc/buscarporid";
	public static final String FUNCC_CONTROLLER_CREAR = "/funcc/crear";
	public static final String FUNCC_CONTROLLER_ELIMINAR = "/funcc/eliminar";

	// Insumo
	public static final String INSUMO_CONTROLLER_BUSCAR_TODOS = "/insumo/buscartodos";
	public static final String INSUMO_CONTROLLER_BUSCAR_POR_ID = "/insumo/buscarporid";
	public static final String INSUMO_CONTROLLER_CREAR = "/insumo/crear";
	public static final String INSUMO_CONTROLLER_ELIMINAR = "/insumo/eliminar";

	// Insumo por Lote
	public static final String INSLOT_CONTROLLER_BUSCAR_TODOS = "/inslot/buscartodos";
	public static final String INSLOT_CONTROLLER_BUSCAR_POR_ID = "/inslot/buscarporid";
	public static final String INSLOT_CONTROLLER_CREAR = "/inslot/crear";
	public static final String INSLOT_CONTROLLER_ELIMINAR = "/inslot/eliminar";

	// Labores
	public static final String LABOR_CONTROLLER_BUSCAR_TODOS = "/labor/buscartodos";
	public static final String LABOR_CONTROLLER_BUSCAR_POR_ID = "/labor/buscarporid";
	public static final String LABOR_CONTROLLER_CREAR = "/labor/crear";
	public static final String LABOR_CONTROLLER_ELIMINAR = "/labor/eliminar";

	// Lote
	public static final String LOTE_CONTROLLER_BUSCAR_TODOS = "/lote/buscartodos";
	public static final String LOTE_CONTROLLER_BUSCAR_POR_ID = "/lote/buscarporid";
	public static final String LOTE_CONTROLLER_CREAR = "/lote/crear";
	public static final String LOTE_CONTROLLER_ELIMINAR = "/lote/eliminar";

	// Movimientos contables
	public static final String MOVCON_CONTROLLER_BUSCAR_TODOS = "/movcon/buscartodos";
	public static final String MOVCON_CONTROLLER_BUSCAR_POR_ID = "/movcon/buscarporid";
	public static final String MOVCON_CONTROLLER_CREAR = "/movcon/crear";
	public static final String MOVCON_CONTROLLER_ELIMINAR = "/movcon/eliminar";

	// Recurso
	public static final String RECUR_CONTROLLER_BUSCAR_TODOS = "/recur/buscartodos";
	public static final String RECUR_CONTROLLER_BUSCAR_POR_ID = "/recur/buscarporid";
	public static final String RECUR_CONTROLLER_CREAR = "/recur/crear";
	public static final String RECUR_CONTROLLER_ELIMINAR = "/recur/eliminar";

	// Referencia
	public static final String REFER_CONTROLLER_BUSCAR_TODOS = "/refer/buscartodos";
	public static final String REFER_CONTROLLER_BUSCAR_POR_ID = "/refer/buscarporid";
	public static final String REFER_CONTROLLER_CREAR = "/refer/crear";
	public static final String REFER_CONTROLLER_ELIMINAR = "/refer/eliminar";

	// Ventas
	public static final String VENTAS_CONTROLLER_BUSCAR_TODOS = "/ventas/buscartodos";
	public static final String VENTAS_CONTROLLER_BUSCAR_POR_ID = "/ventas/buscarporid";
	public static final String VENTAS_CONTROLLER_CREAR = "/ventas/crear";
	public static final String VENTAS_CONTROLLER_ELIMINAR = "/ventas/eliminar";

}
