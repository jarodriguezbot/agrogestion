/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IDescuentoNominaServiceRepo;
import com.sumset.agrogestiondatos.models.DescuentoNomina;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IDescuentoNominaRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class DescuentoNominaServiceRepo
 	extends OperacionesBasicasImpl<DescuentoNomina, Long> implements IDescuentoNominaServiceRepo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IDescuentoNominaRepo descNominaRepo;
	
	@Override
	public CrudRepository<DescuentoNomina, Long> getRepo() {
		return descNominaRepo;
	}


}
