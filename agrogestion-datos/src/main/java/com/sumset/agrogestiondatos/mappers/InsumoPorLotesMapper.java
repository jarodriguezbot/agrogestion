/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestiondatos.models.Insumo;
import com.sumset.agrogestiondatos.models.InsumoPorLote;
import com.sumset.agrogestiondatos.models.Lote;

/**
 * @author joals
 *
 */
@Mapper(uses = {Insumo.class, Lote.class})
public interface InsumoPorLotesMapper {
	
	/**
	 * 
	 * @param insumoPorLote
	 * @return
	 */
	@Mapping(source = "insumoPorLote.id",  target = "id")
	@Mapping(source = "insumoPorLote.ixlFecha",  target = "ixlFecha")
	@Mapping(source = "insumoPorLote.ixlCorte",  target = "ixlCorte")
	@Mapping(source = "insumoPorLote.ixlUnidad",  target = "ixlUnidad")
	@Mapping(source = "insumoPorLote.ixlCantidad",  target = "ixlCantidad")
	@Mapping(source = "insumoPorLote.ixlPrecio",  target = "ixlPrecio")
	@Mapping(source = "insumoPorLote.ixlValorTotal",  target = "ixlValorTotal")
	@Mapping(source = "insumoPorLote.ixlCreditos",  target = "ixlCreditos")
	@Mapping(source = "insumoPorLote.ixlComprobante",  target = "ixlComprobante")
	@Mapping(source = "insumoPorLote.ixlNumeroCortes",  target = "ixlNumeroCortes")
	@Mapping(source = "insumoPorLote.ixlEstadoCultivo",  target = "ixlEstadoCultivo")
	@Mapping(source = "insumoPorLote.ixlCultivo",  target = "ixlCultivo")
	@Mapping(source = "insumoPorLote.ixlMayorLabores",  target = "ixlMayorLabores")
	@Mapping(source = "insumoPorLote.ixlInsId.id",  target = "ixlInsId")
	@Mapping(source = "insumoPorLote.ixlLoteId.id",  target = "ixlLoteId")
	InsumoPorLoteDTO insumoPorLoteADTO(InsumoPorLote insumoPorLote);
	
	/**
	 * 
	 * @param insumoPorLote
	 * @return
	 */
	@Mapping(source = "insumoPorLote.id",  target = "id")
	@Mapping(source = "insumoPorLote.ixlFecha",  target = "ixlFecha")
	@Mapping(source = "insumoPorLote.ixlCorte",  target = "ixlCorte")
	@Mapping(source = "insumoPorLote.ixlUnidad",  target = "ixlUnidad")
	@Mapping(source = "insumoPorLote.ixlCantidad",  target = "ixlCantidad")
	@Mapping(source = "insumoPorLote.ixlPrecio",  target = "ixlPrecio")
	@Mapping(source = "insumoPorLote.ixlValorTotal",  target = "ixlValorTotal")
	@Mapping(source = "insumoPorLote.ixlCreditos",  target = "ixlCreditos")
	@Mapping(source = "insumoPorLote.ixlComprobante",  target = "ixlComprobante")
	@Mapping(source = "insumoPorLote.ixlNumeroCortes",  target = "ixlNumeroCortes")
	@Mapping(source = "insumoPorLote.ixlEstadoCultivo",  target = "ixlEstadoCultivo")
	@Mapping(source = "insumoPorLote.ixlCultivo",  target = "ixlCultivo")
	@Mapping(source = "insumoPorLote.ixlMayorLabores",  target = "ixlMayorLabores")
	@Mapping(source = "insumoPorLote.ixlInsId",  target = "ixlInsId.id")
	@Mapping(source = "insumoPorLote.ixlLoteId",  target = "ixlLoteId.id")
	InsumoPorLote insumoPorLoteDTOAEntidad(InsumoPorLoteDTO insumoPorLote);
	
	/**
	 * 
	 * @param insumoPorLotes
	 * @return
	 */
	List<InsumoPorLoteDTO> insumosPorLotesEntidadesADTO (List<InsumoPorLote> insumoPorLotes);
	
	/**
	 * 
	 * @param insumoPorLotes
	 * @return
	 */
	List<InsumoPorLote> insumosPorLoteDTOAEntidades (List<InsumoPorLoteDTO> insumoPorLotes);
	
	
}
