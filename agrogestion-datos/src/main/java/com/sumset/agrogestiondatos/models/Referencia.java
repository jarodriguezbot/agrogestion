/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_referencia")
public class Referencia {

	/**
	 * 
	 */
	public Referencia() {
		super();
	}

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ref_id")
	private Long id;
	
	@Basic(optional = false)
	@Column(name = "ref_codigo", length = 3)
	private String refCodigo;
	
	@Basic(optional = false)
	@Column(name = "ref_nombre", length = 50)
	private String refNombre;
	
	@Basic(optional = false)
	@Column(name = "ref_descripcion", length = 50)
	private String refDescripcion;
	
	@Basic(optional=false)
	@Column(name = "ref_valor", length = 3)
	private String refValor;
	
	@Basic(optional=false)
	@Column(name = "ref_ref_codigo", length = 3)
	private Long refRefCodigo;
}
