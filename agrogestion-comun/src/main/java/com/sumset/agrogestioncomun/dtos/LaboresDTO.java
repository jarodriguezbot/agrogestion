/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.sql.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LaboresDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public LaboresDTO() {
		super();
	}
	
	private Long id;
	private Long labMayorLabor;
	private Date labFecha;
	private Double labPrecio;
	private Long labCuenta;
	private Long labComprobante;
	private Double labValorTotal;
	private String labEstado;
	private Long labCultivo;
	private Long ccLabId;

}
