/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LoteDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public LoteDTO() {
		super();
	}

	private Long id;
	private String lotNombre;
	private String lotCodigo;
	private Long lotArbolesContados;
	private Double lotAreaBruta;
	private String lotUnidad;
	private Double lotAreaAgricola;
	private String lotCultivo;
	private Long lotChar;
	private Long lotAnioRenovacion;
	private String lotEstado;
	private Double lotCalculoHectarea;
	private Double lotPrecioUnitario;
	private Double lotProducProyectada;
	private Double lotFacturacion;
	private String lotObservaciones;
	private Long lotCcId;
}
