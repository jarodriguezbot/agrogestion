/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CuentaContableDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public CuentaContableDTO() {
		super();
	}

	private Long id;
	private String ctaNombre;
	private String ctaCodDebito;
	private String ctaCodCredito;
	private String ctaTipo;
	private String ctaPuc;
	private String ctaUso;
	private String ctaGrupo;
}
