/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.DescuentoNomina;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IDescuentoNominaRepo extends CrudRepository<DescuentoNomina, Long>{

}
