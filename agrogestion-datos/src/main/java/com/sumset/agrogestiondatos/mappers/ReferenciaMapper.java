/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestiondatos.models.Referencia;

/**
 * @author joals
 *
 */
@Mapper
public interface ReferenciaMapper {
	
	/**
	 * 
	 * @param referencia
	 * @return
	 */
	@Mapping(source = "referencia.id",  target = "id")
	@Mapping(source = "referencia.refCodigo",  target = "refCodigo")
	@Mapping(source = "referencia.refNombre",  target = "refNombre")
	@Mapping(source = "referencia.refDescripcion",  target = "refDescripcion")
	@Mapping(source = "referencia.refValor",  target = "refValor")
	@Mapping(source = "referencia.refRefCodigo",  target = "refRefCodigo")
	ReferenciaDTO referenciaADTO(Referencia referencia);
	
	/**
	 * 
	 * @param referencia
	 * @return
	 */
	@Mapping(source = "referencia.id",  target = "id")
	@Mapping(source = "referencia.refCodigo",  target = "refCodigo")
	@Mapping(source = "referencia.refNombre",  target = "refNombre")
	@Mapping(source = "referencia.refDescripcion",  target = "refDescripcion")
	@Mapping(source = "referencia.refValor",  target = "refValor")
	@Mapping(source = "referencia.refRefCodigo",  target = "refRefCodigo")
	Referencia referenciaDTOAEntidad(ReferenciaDTO referencia);
	
	/**
	 * 
	 * @param referencias
	 * @return
	 */
	List<ReferenciaDTO> referenciasEntidadesADTO(List<Referencia> referencias);
	
	/**
	 * 
	 * @param referencias
	 * @return
	 */
	List<Referencia> referenciasDTOAEntidades(List<ReferenciaDTO> referencias);

}
