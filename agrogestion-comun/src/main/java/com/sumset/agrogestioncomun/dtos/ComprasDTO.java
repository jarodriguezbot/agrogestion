/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.sql.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ComprasDTO extends RespuestaDTO{

	/**
	 * 
	 */
	public ComprasDTO() {
		super();
	}

	private Long id;
	private String cmpComprobante;
	private Date cmpFecha;
	private Long cmpCorte;
	private String cmpTercero;
	private String cmpDescripcion;
	private Double cmpCantidad;
	private String cmpUnidad;
	private String cmpRecurso;
	private Double cmpPrecio;
	private String cmpDebito;
	private Double cmpValorTotal;
	private String cmpCodDebito;
	private String cmpTipoGastDebito;
	private String cmpTipoGastCredito;
	private Long cmpCcId;
}
