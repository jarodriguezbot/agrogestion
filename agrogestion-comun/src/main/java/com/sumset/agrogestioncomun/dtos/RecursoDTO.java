/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class RecursoDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public RecursoDTO() {
		super();
	}

	private Long id;
	private String recNombre;
	private String recDescripcion;
	private String recUnidad;
	private Double recPrecio;
	private Long recCcId;
}
