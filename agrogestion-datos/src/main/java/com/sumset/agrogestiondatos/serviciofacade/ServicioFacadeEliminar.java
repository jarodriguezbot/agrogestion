/**
 * 
 */
package com.sumset.agrogestiondatos.serviciofacade;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.RespuestaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGConstantes;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.models.CuentaContable;
import com.sumset.agrogestiondatos.models.DescuentoNomina;
import com.sumset.agrogestiondatos.models.Empresa;
import com.sumset.agrogestiondatos.models.Funcionario;
import com.sumset.agrogestiondatos.models.FuncionarioPorCentroCosto;
import com.sumset.agrogestiondatos.models.Insumo;
import com.sumset.agrogestiondatos.models.InsumoPorLote;
import com.sumset.agrogestiondatos.models.Labores;
import com.sumset.agrogestiondatos.models.Lote;
import com.sumset.agrogestiondatos.models.MovimientosContables;
import com.sumset.agrogestiondatos.models.Recurso;
import com.sumset.agrogestiondatos.models.Referencia;
import com.sumset.agrogestiondatos.models.Ventas;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Component
public class ServicioFacadeEliminar extends ServicioFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ServicioFacadeEliminar() {
		super();
	}

	/**
	 * Método para eliminar un centro de costo
	 * @param centroDeCostoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		centroCostoDAO.eliminar(centroDeCostoDTO.getId());
		CentroDeCosto centroDeCosto = centroCostoDAO.obtener(centroDeCostoDTO.getId());
		if (centroDeCosto != null && centroDeCosto.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Centro de costo eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Centro de costo No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar una compra
	 * @param comprasDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		comprasDAO.eliminar(comprasDTO.getId());
		Compras compras = comprasDAO.obtener(comprasDTO.getId());
		if (compras != null && compras.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Compra eliminada correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("La compra No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar una cuenta contable
	 * @param cuentaContableDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		cuentaContableDAO.eliminar(cuentaContableDTO.getId());
		CuentaContable cuentaContable = cuentaContableDAO.obtener(cuentaContableDTO.getId());
		if (cuentaContable != null && cuentaContable.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Cuenta Contable eliminada correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("La Cuenta Contable No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un descuento de nómina
	 * @param descuentoNominaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		descuentoNominaDAO.eliminar(descuentoNominaDTO.getId());
		DescuentoNomina descuentoNomina = descuentoNominaDAO.obtener(descuentoNominaDTO.getId());
		if (descuentoNomina != null && descuentoNomina.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Descuento de Nómina eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Descuento de Nómina No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar una empresa
	 * @param empresaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		empresaDAO.eliminar(empresaDTO.getId());
		Empresa empresa = empresaDAO.obtener(empresaDTO.getId());
		if (empresa != null && empresa.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Empresa eliminada correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("La Empresa No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un funcionario por centro de costo
	 * @param funcionarioPorCentroCostoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarFuncionarioPorCentroCosto(FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO)
			throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		funcionarioPorCentroCostoDAO.eliminar(funcionarioPorCentroCostoDTO.getId());
		FuncionarioPorCentroCosto funcionarioPorCentroCosto = funcionarioPorCentroCostoDAO.obtener(funcionarioPorCentroCostoDTO.getId());
		if (funcionarioPorCentroCosto != null && funcionarioPorCentroCosto.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionario por Centro de Costo eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Funcionario por Centro de Costo No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un funcionario
	 * @param funcionarioDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		funcionarioDAO.eliminar(funcionarioDTO.getId());
		Funcionario funcionario = funcionarioDAO.obtener(funcionarioDTO.getId());
		if (funcionario != null && funcionario.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Funcionario eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Funcionario No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un insumo por lote
	 * @param insumoPorLoteDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		insumoPorLoteDAO.eliminar(insumoPorLoteDTO.getId());
		InsumoPorLote insumoPorLote = insumoPorLoteDAO.obtener(insumoPorLoteDTO.getId()); 
		if (insumoPorLote != null && insumoPorLote.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumo por Lote eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("Insumo por Lote No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un insumo
	 * @param insumoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		insumoDAO.eliminar(insumoDTO.getId());
		Insumo insumo = insumoDAO.obtener(insumoDTO.getId()); 
		if (insumo != null && insumo.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Insumo eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("Insumo No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar una labor
	 * @param laboresDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarLabores(LaboresDTO laboresDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		laboresDAO.eliminar(laboresDTO.getId());
		Labores labores = laboresDAO.obtener(laboresDTO.getId());
		if (labores != null && labores.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Labor eliminada correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("La Labor No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un lote
	 * @param loteDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarLote(LoteDTO loteDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		loteDAO.eliminar(loteDTO.getId());
		Lote lote = loteDAO.obtener(loteDTO.getId());
		if (lote != null && lote.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Lote eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Lote No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un movimiento contable
	 * @param movimientosContablesDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		movimientosContablesDAO.eliminar(movimientosContablesDTO.getId());
		MovimientosContables movimientosContables = movimientosContablesDAO.obtener(movimientosContablesDTO.getId());
		if (movimientosContables != null && movimientosContables.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Movimiento Contable eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Movimiento Contable No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar un recurso
	 * @param recursoDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		recursoDAO.eliminar(recursoDTO.getId());
		Recurso recurso = recursoDAO.obtener(recursoDTO.getId());
		if (recurso != null && recurso.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("El Recurso eliminado correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("El Recurso No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar una referencia
	 * @param referenciaDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		referenciaDAO.eliminar(referenciaDTO.getId());
		Referencia referencia = referenciaDAO.obtener(referenciaDTO.getId());
		if (referencia != null && referencia.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Referencia eliminada correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("La Referencia No se ha eliminado");
		}
		return respuesta;
	}

	/**
	 * Método para eliminar una venta
	 * @param ventasDTO
	 * @throws AGExcepciones
	 */
	public RespuestaDTO eliminarVentas(VentasDTO ventasDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		ventasDAO.eliminar(ventasDTO.getId());
		Ventas ventas = ventasDAO.obtener(ventasDTO.getId());
		if (ventas != null && ventas.getId() != null) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXITOSA);
			respuesta.setMensajeRespuesta("Ventaeliminada correctamente");
		} else {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_ERROR);
			respuesta.setMensajeRespuesta("La Venta No se ha eliminado");
		}
		return respuesta;
	}

}
