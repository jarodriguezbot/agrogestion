/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class InsumoDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public InsumoDTO() {
		super();
	}

	private Long id;
	private String ismNombre;
	private String ismUnidad;
	private Double ismPrecio;
}
