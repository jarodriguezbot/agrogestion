/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.sql.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class MovimientosContablesDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public MovimientosContablesDTO() {
		super();
	}
	
	private Long id;
	private Date mvtFecha;
	private String mvtOperacion;
	private Double mvtValorCredito;
	private Double mvtValorDebito;
	private String mvtComprobante;
	private Long cuentaContId;
	private Long centoCostoId;

}
