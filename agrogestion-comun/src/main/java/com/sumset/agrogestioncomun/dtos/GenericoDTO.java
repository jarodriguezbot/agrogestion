/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class GenericoDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public GenericoDTO() {
		super();
	}
	
	private List<Object> listaObjetos;
}
