/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.models.Empresa;

/**
 * @author joals
 *
 */
@Mapper(uses = {Empresa.class})
public interface CentroCostoMapper {
	
	/**
	 * 
	 * @param centroDeCosto
	 * @return
	 */
	@Mapping(source = "centroDeCosto.id", target = "id")
	@Mapping(source = "centroDeCosto.ccNombre", target = "ccNombre")
	@Mapping(source = "centroDeCosto.ccDescripcion", target = "ccDescripcion")
	@Mapping(source = "centroDeCosto.empresa.id", target = "ccEmpId")
	CentroDeCostoDTO centroDeCostoADTO(CentroDeCosto centroDeCosto);
	
	/**
	 * 
	 * @param centroDeCosto
	 * @return
	 */
	@Mapping(source = "centroDeCosto.id", target = "id")
	@Mapping(source = "centroDeCosto.ccNombre", target = "ccNombre")
	@Mapping(source = "centroDeCosto.ccDescripcion", target = "ccDescripcion")
	@Mapping(source = "centroDeCosto.ccEmpId", target = "empresa.id")
	CentroDeCosto centroDeCostoDTOAEntidad(CentroDeCostoDTO centroDeCosto);
	
	/**
	 * 
	 * @param centroDeCostos
	 * @return
	 */
	List<CentroDeCostoDTO> centroDeCostosEntidadesADTO(List<CentroDeCosto> centroDeCostos);
	
	/**
	 * 
	 * @param centroDeCostosDTO
	 * @return
	 */
	List<CentroDeCosto> centroDeCostosDTOAEntidades(List<CentroDeCostoDTO> centroDeCostosDTO);
}
