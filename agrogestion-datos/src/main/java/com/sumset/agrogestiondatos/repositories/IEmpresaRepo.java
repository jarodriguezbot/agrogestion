/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Empresa;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IEmpresaRepo extends CrudRepository<Empresa, Long>{

}
