/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CentroDeCostoDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public CentroDeCostoDTO() {
		super();
	}

	private Long id;
	private String ccNombre;
	private String ccDescripcion;
	private Long ccEmpId;
}
