/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Referencia;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IReferenciaRepo extends CrudRepository<Referencia, Long>{

}
