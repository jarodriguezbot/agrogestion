/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.models.Empresa;

/**
 * @author joals
 *
 */
@Mapper
public interface EmpresaMapper {
	
	/**
	 * 
	 * @param empresa
	 * @return
	 */
	@Mapping(source = "empresa.id",  target = "id")
	@Mapping(source = "empresa.empNombre",  target = "empNombre")
	@Mapping(source = "empresa.empDireccion",  target = "empDireccion")
	EmpresaDTO empresaADTO(Empresa empresa);
	
	/**
	 * 
	 * @param empresa
	 * @return
	 */
	@Mapping(source = "empresa.id",  target = "id")
	@Mapping(source = "empresa.empNombre",  target = "empNombre")
	@Mapping(source = "empresa.empDireccion",  target = "empDireccion")
	Empresa empresaDTOAEntidad(EmpresaDTO empresa);
	
	/**
	 * 
	 * @param empresas
	 * @return
	 */
	List<EmpresaDTO> empresasEntitiesADTO(List<Empresa> empresas);
	
	/**
	 * 
	 * @param empresas
	 * @return
	 */
	List<Empresa> empresasDTOAEntities(List<EmpresaDTO> empresas);
}
