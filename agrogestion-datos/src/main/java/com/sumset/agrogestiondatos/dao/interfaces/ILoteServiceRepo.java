/**
 * 
 */
package com.sumset.agrogestiondatos.dao.interfaces;

import java.io.Serializable;

import com.sumset.agrogestiondatos.models.Lote;
import com.sumset.agrogestiondatos.operacionesbasicas.interfaces.IOperacionesBasicas;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface ILoteServiceRepo extends IOperacionesBasicas<Lote, Long>, Serializable{

}
