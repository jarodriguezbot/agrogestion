/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IVentasServiceRepo;
import com.sumset.agrogestiondatos.models.Ventas;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IVentasRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class VentasServiceRepo 
	extends OperacionesBasicasImpl<Ventas, Long> implements IVentasServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IVentasRepo ventasRepo;
	
	@Override
	public CrudRepository<Ventas, Long> getRepo() {
		return ventasRepo;
	}


}
