/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.ILoteServiceRepo;
import com.sumset.agrogestiondatos.models.Lote;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.ILoteRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class LoteServiceRepo  
 	extends OperacionesBasicasImpl<Lote, Long> implements ILoteServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ILoteRepo loteRepo;
	
	@Override
	public CrudRepository<Lote, Long> getRepo() {
		return loteRepo;
	}


}
