/**
 * 
 */
package com.sumset.agrogestiondatos.dao.interfaces;

import java.io.Serializable;

import com.sumset.agrogestiondatos.models.Compras;
import com.sumset.agrogestiondatos.operacionesbasicas.interfaces.IOperacionesBasicas;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IComprasServiceRepo extends IOperacionesBasicas<Compras, Long>, Serializable{

}
