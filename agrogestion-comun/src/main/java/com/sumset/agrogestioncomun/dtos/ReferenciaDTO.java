/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ReferenciaDTO extends RespuestaDTO {

	/**
	 * 
	 */
	public ReferenciaDTO() {
		super();
	}

	private Long id;
	private String refCodigo;
	private String refNombre;
	private String refDescripcion;
	private String refValor;
	private Long refRefCodigo;
}
