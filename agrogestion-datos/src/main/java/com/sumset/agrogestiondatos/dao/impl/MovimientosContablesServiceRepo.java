/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IMovimientosContablesServiceRepo;
import com.sumset.agrogestiondatos.models.MovimientosContables;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IMovimientosContablesRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class MovimientosContablesServiceRepo 
	extends OperacionesBasicasImpl<MovimientosContables, Long> implements IMovimientosContablesServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IMovimientosContablesRepo movContablesRepo;
	
	@Override
	public CrudRepository<MovimientosContables, Long> getRepo() {
		return movContablesRepo;
	}


}
