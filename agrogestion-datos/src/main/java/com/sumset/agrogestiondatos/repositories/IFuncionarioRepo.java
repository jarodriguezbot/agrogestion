/**
 * 
 */
package com.sumset.agrogestiondatos.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sumset.agrogestiondatos.models.Funcionario;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IFuncionarioRepo extends CrudRepository<Funcionario, Long>{

}
