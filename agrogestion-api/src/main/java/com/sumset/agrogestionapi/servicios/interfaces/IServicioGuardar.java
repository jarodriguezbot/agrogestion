/**
 * 
 */
package com.sumset.agrogestionapi.servicios.interfaces;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;

/**
 * @author Sumset
 *
 */
public interface IServicioGuardar {

	/**
	 * Método que guarda un centro de costo en la BD
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO guardarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método para modificar un centro de costo
	 * 
	 * @param centroDeCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CentroDeCostoDTO modificarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una compra
	 * 
	 * @param comprasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ComprasDTO guardarCompra(ComprasDTO comprasDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una cuenta contable
	 * 
	 * @param cuentaContableDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public CuentaContableDTO guardarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un descuento de nómina
	 * 
	 * @param descuentoNominaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public DescuentoNominaDTO guardarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una empresa
	 * 
	 * @param empresaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public EmpresaDTO guardarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un funcionario por centro de costo
	 * 
	 * @param funcionarioPorCentroCostoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioPorCentroCostoDTO guardarFuncionarioPorCentroCosto(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un funcionario
	 * 
	 * @param funcionarioDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public FuncionarioDTO guardarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un lote
	 * 
	 * @param insumoPorLoteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoPorLoteDTO guardarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un insumo
	 * 
	 * @param insumoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public InsumoDTO guardarInsumo(InsumoDTO insumoDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una labor
	 * 
	 * @param laboresDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LaboresDTO guardarLabor(LaboresDTO laboresDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un lote
	 * 
	 * @param loteDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public LoteDTO guardarLote(LoteDTO loteDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar un movimiento contable
	 * 
	 * @param movimientosContablesDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public MovimientosContablesDTO guardarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones;

	/**
	 * Método para agregar o modificar un recurso
	 * 
	 * @param recursoDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public RecursoDTO guardarRecurso(RecursoDTO recursoDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una referencia
	 * 
	 * @param referenciaDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public ReferenciaDTO guardarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones;

	/**
	 * Método para agregar o modificar una venta
	 * 
	 * @param ventasDTO
	 * @return
	 * @throws AGExcepciones
	 */
	public VentasDTO guardarVenta(VentasDTO ventasDTO) throws AGExcepciones;

}
