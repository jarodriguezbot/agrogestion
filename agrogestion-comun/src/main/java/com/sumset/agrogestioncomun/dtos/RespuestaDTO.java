/**
 * 
 */
package com.sumset.agrogestioncomun.dtos;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
public class RespuestaDTO {

	/**
	 * 
	 */
	public RespuestaDTO() {
		super();
	}

	private int codigoRespuesta;
	private String mensajeRespuesta;
}
