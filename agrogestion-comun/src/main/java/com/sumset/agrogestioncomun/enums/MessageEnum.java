/**
 * 
 */
package com.sumset.agrogestioncomun.enums;

/**
 * @author Agustín Palomino Pardo
 *
 */
public enum MessageEnum {
	// File normal messages
	MESSAGES("mensajes.mensajes"),

	// File with errors messages
	ERRORS("mensajes.errores");

	// Package name of messages
	private String packageName;

	/**
	 * 
	 * @param packageName
	 */
	MessageEnum(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * 
	 * @return
	 */
	public String getPackageName() {
		return packageName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return packageName;
	}
}
