/**
 * 
 */
package com.sumset.agrogestiondatos.sessionfacade.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.GenericoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.RespuestaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGLogger;
import com.sumset.agrogestiondatos.serviciofacade.ServicioFacadeConsulta;
import com.sumset.agrogestiondatos.serviciofacade.ServicioFacadeEliminar;
import com.sumset.agrogestiondatos.serviciofacade.ServicioFacadeGuardar;
import com.sumset.agrogestiondatos.sessionfacade.interfaces.ISessionFacade;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Service
public class SessionFacade implements ISessionFacade, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired 
	ServicioFacadeConsulta facadeConsulta;
	
	@Autowired
	ServicioFacadeGuardar facadeGuardar;
	
	@Autowired
	ServicioFacadeEliminar facadeEliminar;
	
	@Autowired
	AGLogger logger;
	
	
	/**
	 * 
	 */
	public SessionFacade() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public GenericoDTO buscarTodosCentroCostos() throws AGExcepciones {
		return facadeConsulta.buscarTodosCentroCostos();
	}

	@Override
	public CentroDeCostoDTO buscarCentroCostoPorId(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		return facadeConsulta.buscarCentroCostoPorId(centroDeCostoDTO);
	}

	@Override
	public CentroDeCostoDTO guardarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		return null;
	}

	@Override
	public CentroDeCostoDTO modificarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarCentroCosto(CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodasCompras() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ComprasDTO buscarCompraPorId(ComprasDTO comprasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ComprasDTO guardarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodasCuentasContables() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CuentaContableDTO buscarCuentaContablePorId(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CuentaContableDTO guardarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosDescuentosNomina() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DescuentoNominaDTO buscarDescuentoNominaPorId(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DescuentoNominaDTO guardarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodasEmpresas() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmpresaDTO buscarEmpresaPorId(EmpresaDTO empresaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmpresaDTO guardarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosFuncionariosPorCentroCosto() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FuncionarioPorCentroCostoDTO buscarFuncionarioPorCentroCostoPorId(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarFuncionarioPorCentroCosto(FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO)
			throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FuncionarioPorCentroCostoDTO guardarFuncionarioPorCentroCosto(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosFuncionarios() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FuncionarioDTO buscarFuncionarioPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FuncionarioDTO guardarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosInsumosPorLote() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InsumoPorLoteDTO buscarInsumosPorLotePorId(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InsumoPorLoteDTO guardarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosInsumos() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InsumoDTO buscarInsumosPorId(InsumoDTO insumoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InsumoDTO guardarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodasLabores() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LaboresDTO buscarLaboresPorId(LaboresDTO laboresDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarLabores(LaboresDTO laboresDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LaboresDTO guardarLabor(LaboresDTO laboresDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosLotes() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoteDTO buscarLotePorId(LoteDTO loteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarLote(LoteDTO loteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoteDTO guardarLote(LoteDTO loteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosMovimientosContables() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MovimientosContablesDTO buscarMovimientosContablesPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MovimientosContablesDTO guardarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodosRecursos() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RecursoDTO buscarRecursoPorId(RecursoDTO recursoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RecursoDTO guardarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodasReferencias() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReferenciaDTO buscarReferenciaPorId(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReferenciaDTO guardarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericoDTO buscarTodasVentas() throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VentasDTO buscarVentasPorId(VentasDTO ventasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarVentas(VentasDTO ventasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VentasDTO guardarVenta(VentasDTO ventasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

}
