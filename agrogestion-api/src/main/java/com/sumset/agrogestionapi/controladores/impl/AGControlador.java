/**
 * 
 */
package com.sumset.agrogestionapi.controladores.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sumset.agrogestionapi.controladores.interfaces.IAGControlador;
import com.sumset.agrogestionapi.servicios.interfaces.IServicioConsultar;
import com.sumset.agrogestionapi.servicios.interfaces.IServicioEliminar;
import com.sumset.agrogestionapi.servicios.interfaces.IServicioGuardar;
import com.sumset.agrogestioncomun.dtos.CentroDeCostoDTO;
import com.sumset.agrogestioncomun.dtos.ComprasDTO;
import com.sumset.agrogestioncomun.dtos.CuentaContableDTO;
import com.sumset.agrogestioncomun.dtos.DescuentoNominaDTO;
import com.sumset.agrogestioncomun.dtos.EmpresaDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioDTO;
import com.sumset.agrogestioncomun.dtos.FuncionarioPorCentroCostoDTO;
import com.sumset.agrogestioncomun.dtos.GenericoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestioncomun.dtos.InsumoPorLoteDTO;
import com.sumset.agrogestioncomun.dtos.LaboresDTO;
import com.sumset.agrogestioncomun.dtos.LoteDTO;
import com.sumset.agrogestioncomun.dtos.MovimientosContablesDTO;
import com.sumset.agrogestioncomun.dtos.RecursoDTO;
import com.sumset.agrogestioncomun.dtos.ReferenciaDTO;
import com.sumset.agrogestioncomun.dtos.RespuestaDTO;
import com.sumset.agrogestioncomun.dtos.VentasDTO;
import com.sumset.agrogestioncomun.excepciones.AGExcepciones;
import com.sumset.agrogestioncomun.utils.AGLogger;
import com.sumset.agrogestioncomun.utils.AGConstantes;

/**
 * @author Sumset
 *
 */
@RestController
@CrossOrigin("*")
@RequestMapping(AGConstantes.WEB_CONTROLLER_GET_HOME)
public class AGControlador implements IAGControlador {

	@Autowired
	IServicioConsultar consultar;
	
	@Autowired
	IServicioEliminar eliminar;
	
	@Autowired
	IServicioGuardar guardar;
	
	@Autowired
	AGLogger logger;
	
	/**
	 * 
	 */
	public AGControlador() {
		super();
	}

	@GetMapping(AGConstantes.CENCOS_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosCentroCostos() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosCentroCostos();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.CENCOS_CONTROLLER_BUSCAR_POR_ID)
	@Override
	public CentroDeCostoDTO buscarCentroCostoPorId(@RequestBody CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		CentroDeCostoDTO respuesta = new CentroDeCostoDTO();
		try {
			respuesta = consultar.buscarCentroCostoPorId(centroDeCostoDTO);
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.CENCOS_CONTROLLER_CREAR)
	@Override
	public CentroDeCostoDTO guardarCentroCosto(@RequestBody CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		CentroDeCostoDTO respuesta = new CentroDeCostoDTO();
		try {
			respuesta = guardar.guardarCentroCosto(centroDeCostoDTO);
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.CENCOS_CONTROLLER_MODIFICAR)
	@Override
	public CentroDeCostoDTO modificarCentroCosto(@RequestBody CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		CentroDeCostoDTO respuesta = new CentroDeCostoDTO();
		try {
			respuesta = guardar.modificarCentroCosto(centroDeCostoDTO);
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.CENCOS_CONTROLLER_ELIMINAR)
	@Override
	public RespuestaDTO eliminarCentroCosto(@RequestBody CentroDeCostoDTO centroDeCostoDTO) throws AGExcepciones {
		RespuestaDTO respuesta = new RespuestaDTO();
		try {
			respuesta = eliminar.eliminarCentroCosto(centroDeCostoDTO);
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@GetMapping(AGConstantes.COMPRAS_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodasCompras() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodasCompras();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.COMPRAS_CONTROLLER_BUSCAR_POR_ID)
	@Override
	public ComprasDTO buscarCompraPorId(ComprasDTO comprasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(AGConstantes.COMPRAS_CONTROLLER_ELIMINAR)
	@Override
	public RespuestaDTO eliminarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(AGConstantes.COMPRAS_CONTROLLER_CREAR)
	@Override
	public ComprasDTO guardarCompra(ComprasDTO comprasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.CUCONT_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodasCuentasContables() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodasCuentasContables();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.CUCONT_CONTROLLER_BUSCAR_POR_ID)
	@Override
	public CuentaContableDTO buscarCuentaContablePorId(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(AGConstantes.CUCONT_CONTROLLER_ELIMINAR)
	@Override
	public RespuestaDTO eliminarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(AGConstantes.CUCONT_CONTROLLER_CREAR)
	@Override
	public CuentaContableDTO guardarCuentaContable(CuentaContableDTO cuentaContableDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.DESNOM_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosDescuentosNomina() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosDescuentosNomina();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@PostMapping(AGConstantes.DESNOM_CONTROLLER_BUSCAR_POR_ID)
	@Override
	public DescuentoNominaDTO buscarDescuentoNominaPorId(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(AGConstantes.DESNOM_CONTROLLER_ELIMINAR)
	@Override
	public RespuestaDTO eliminarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(AGConstantes.DESNOM_CONTROLLER_CREAR)
	@Override
	public DescuentoNominaDTO guardarDescuentoNomina(DescuentoNominaDTO descuentoNominaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.EMPRES_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodasEmpresas() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodasEmpresas();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public EmpresaDTO buscarEmpresaPorId(EmpresaDTO empresaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmpresaDTO guardarEmpresa(EmpresaDTO empresaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.FUNCC_CONTROLLER_BUSCAR_POR_ID)
	@Override
	public GenericoDTO buscarTodosFuncionariosPorCentroCosto() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosFuncionariosPorCentroCosto();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public FuncionarioPorCentroCostoDTO buscarFuncionarioPorCentroCostoPorId(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarFuncionarioPorCentroCosto(FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO)
			throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FuncionarioPorCentroCostoDTO guardarFuncionarioPorCentroCosto(
			FuncionarioPorCentroCostoDTO funcionarioPorCentroCostoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.FUNCIO_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosFuncionarios() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosFuncionarios();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public FuncionarioDTO buscarFuncionarioPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FuncionarioDTO guardarFuncionario(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.INSLOT_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosInsumosPorLote() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosInsumosPorLote();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public InsumoPorLoteDTO buscarInsumosPorLotePorId(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InsumoPorLoteDTO guardarInsumoPorLote(InsumoPorLoteDTO insumoPorLoteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.INSUMO_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosInsumos() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosInsumos();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public InsumoDTO buscarInsumosPorId(InsumoDTO insumoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InsumoDTO guardarInsumo(InsumoDTO insumoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.LABOR_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodasLabores() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodasLabores();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public LaboresDTO buscarLaboresPorId(LaboresDTO laboresDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarLabores(LaboresDTO laboresDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LaboresDTO guardarLabor(LaboresDTO laboresDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.LOTE_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosLotes() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosLotes();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public LoteDTO buscarLotePorId(LoteDTO loteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarLote(LoteDTO loteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoteDTO guardarLote(LoteDTO loteDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.MOVCON_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosMovimientosContables() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosMovimientosContables();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public MovimientosContablesDTO buscarMovimientosContablesPorId(FuncionarioDTO funcionarioDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MovimientosContablesDTO guardarMovimientoContable(MovimientosContablesDTO movimientosContablesDTO)
			throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.RECUR_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodosRecursos() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodosRecursos();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public RecursoDTO buscarRecursoPorId(RecursoDTO recursoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RecursoDTO guardarRecurso(RecursoDTO recursoDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.REFER_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodasReferencias() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodasReferencias();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public ReferenciaDTO buscarReferenciaPorId(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReferenciaDTO guardarReferencia(ReferenciaDTO referenciaDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@GetMapping(AGConstantes.VENTAS_CONTROLLER_BUSCAR_TODOS)
	@Override
	public GenericoDTO buscarTodasVentas() throws AGExcepciones {
		GenericoDTO respuesta = new GenericoDTO();
		try {
			respuesta = consultar.buscarTodasVentas();
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(AGConstantes.CODIGO_RESPUESTA_EXCEPTION);
			respuesta.setMensajeRespuesta("Se ha producido una Excepcion");
		}
		return respuesta;
	}

	@Override
	public VentasDTO buscarVentasPorId(VentasDTO ventasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaDTO eliminarVentas(VentasDTO ventasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VentasDTO guardarVenta(VentasDTO ventasDTO) throws AGExcepciones {
		// TODO Auto-generated method stub
		return null;
	}

}
