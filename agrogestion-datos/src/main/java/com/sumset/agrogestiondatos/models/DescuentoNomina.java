/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_dto_nomina")
public class DescuentoNomina {

	/**
	 * 
	 */
	public DescuentoNomina() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dno_id")
	private Long id;
	
	@Basic(optional = false)
	@Column(name = "dno_fecha")
	private Date dnoFecha;
	
	@Basic(optional = false)
	@Column(name = "dno_corte")
	private Long dnoCorte;

	@Basic(optional = false)
	@Column(name = "dno_tipo_desc", length = 255)
	private String dnoTipoDescuento;
	
	@Basic(optional = false)
	@Column(name = "dno_nombre_func", length = 40)
	private String dnoNombreFuncionario;
	
	@Basic(optional = false)
	@Column(name = "dno_cedula_func", length = 20)
	private String dnoCedulaFuncionario;

	@Basic(optional = false)
	@Column(name = "dno_cantidad")
	private Long dnoCantidad;
	
	@Basic(optional = false)
	@Column(name = "dno_precio")
	private Double dnoPrecio;
	
	@Basic(optional = false)
	@Column(name = "dno_valor_total")
	private Double dnoValorTotal;
	
	@Basic(optional = false)
	@Column(name = "dno_debito", length = 255)
	private String dnoDebito;
	
	@Basic(optional = false)
	@Column(name = "dno_comprobante", length = 255)
	private String dnoComprobante;
	
	@Basic(optional = false)
	@Column(name = "dno_num_comprob")
	private Long dnoNumeroComprobante;
	
	@JoinColumn(name = "dno_fun_id", referencedColumnName = "fun_id")
	@ManyToOne
	private Funcionario dnoFuncId;
	
	@JoinColumn(name = "dno_lot_id", referencedColumnName = "lot_id")
	@ManyToOne
	private Lote dnoLoteId;
}
