/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_funcionario")
public class Funcionario {

	/**
	 * 
	 */
	public Funcionario() {
		super();
	}

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fun_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "fun_cedula", length = 20)
	private String funCedula;
	
	@Basic(optional = false)
	@Column(name = "fun_nombre", length = 40)
	private String funNombre;
	
	@Basic(optional = false)
	@Column(name = "fun_apellido", length = 255)
	private String funApellido;
	
	@Basic(optional = true)
	@Column(name = "fun_celular", length = 20)
	private String funCelular;
	
	@Basic(optional = true)
	@Column(name = "fun_correo", length = 100)
	private String funCorreo;
}
