/**
 * 
 */
package com.sumset.agrogestiondatos.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sumset.agrogestioncomun.dtos.InsumoDTO;
import com.sumset.agrogestiondatos.models.Insumo;

/**
 * @author joals
 *
 */
@Mapper
public interface InsumoMapper {
	
	/**
	 * 
	 * @param insumo
	 * @return
	 */
	@Mapping(source = "insumo.id",  target = "id")
	@Mapping(source = "insumo.ismNombre",  target = "ismNombre")
	@Mapping(source = "insumo.ismUnidad",  target = "ismUnidad")
	@Mapping(source = "insumo.ismPrecio",  target = "ismPrecio")
	InsumoDTO insumoADTO(Insumo insumo);
	
	/**
	 * 
	 * @param insumo
	 * @return
	 */
	@Mapping(source = "insumo.id",  target = "id")
	@Mapping(source = "insumo.ismNombre",  target = "ismNombre")
	@Mapping(source = "insumo.ismUnidad",  target = "ismUnidad")
	@Mapping(source = "insumo.ismPrecio",  target = "ismPrecio")
	Insumo insumoDTOAEntidad(InsumoDTO insumo);
	
	/**
	 * 
	 * @param insumos
	 * @return
	 */
	List<InsumoDTO> insumosEntidadesADTO(List<Insumo>insumos);
	
	/**
	 * 
	 * @param insumos
	 * @return
	 */
	List<Insumo> insumosDTOAEntidades(List<InsumoDTO>insumos);
	
}
