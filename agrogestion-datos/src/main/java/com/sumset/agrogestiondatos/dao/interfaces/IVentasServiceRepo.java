/**
 * 
 */
package com.sumset.agrogestiondatos.dao.interfaces;

import java.io.Serializable;

import com.sumset.agrogestiondatos.models.Ventas;
import com.sumset.agrogestiondatos.operacionesbasicas.interfaces.IOperacionesBasicas;

/**
 * @author Agustín Palomino Pardo
 *
 */
public interface IVentasServiceRepo extends IOperacionesBasicas<Ventas, Long>, Serializable{

}
