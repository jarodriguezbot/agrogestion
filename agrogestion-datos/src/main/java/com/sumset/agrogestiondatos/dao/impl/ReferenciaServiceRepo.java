/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.IReferenciaServiceRepo;
import com.sumset.agrogestiondatos.models.Referencia;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.IReferenciaRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class ReferenciaServiceRepo 
	extends OperacionesBasicasImpl<Referencia, Long> implements IReferenciaServiceRepo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IReferenciaRepo referenciaRepo;
	
	@Override
	public CrudRepository<Referencia, Long> getRepo() {
		return referenciaRepo;
	}


}
