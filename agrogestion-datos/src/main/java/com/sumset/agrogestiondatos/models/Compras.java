/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_compras")
public class Compras {

	/**
	 * 
	 */
	public Compras() {
		super();
	}
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cmp_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "cmp_comprobante", length = 255)
	private String cmpComprobante;

	@Basic(optional = false)
	@Column(name = "cmp_fecha")
	private Date cmpFecha;
	
	@Basic(optional = false)
	@Column(name = "cmp_corte")
	private Long cmpCorte;
	
	@Basic(optional = false)
	@Column(name = "cmp_tercero", length = 255)
	private String cmpTercero;
	
	@Basic(optional = false)
	@Column(name = "cmp_descrip", length = 255)
	private String cmpDescripcion;
	
	@Basic(optional = false)
	@Column(name = "cmp_cantidad")
	private Double cmpCantidad;
	
	@Basic(optional = false)
	@Column(name = "cmp_unidad", length = 10)
	private String cmpUnidad;
	
	@Basic(optional = false)
	@Column(name = "cmp_recurso", length = 255)
	private String cmpRecurso;
	
	@Basic(optional = false)
	@Column(name = "cmp_precio")
	private Double cmpPrecio;
	
	@Basic(optional = false)
	@Column(name = "cmp_debito", length = 255)
	private String cmpDebito;
	
	@Basic(optional = false)
	@Column(name = "cmp_valor_total")
	private Double cmpValorTotal;
	
	@Basic(optional = false)
	@Column(name = "cmp_cod_debito", length = 255)
	private String cmpCodDebito;
	
	@Basic(optional = false)
	@Column(name = "cmp_tipo_gast_deb", length = 255)
	private String cmpTipoGastDebito;
	
	@Basic(optional = false)
	@Column(name = "cmp_tipo_gast_cred", length = 255)
	private String cmpTipoGastCredito;
	
	@JoinColumn(name = "cmp_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centroCosto;
}
