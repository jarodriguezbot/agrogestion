/**
 * 
 */
package com.sumset.agrogestiondatos.models;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Data
@Entity
@Table(name = "ag_ventas")
public class Ventas {

	/**
	 * 
	 */
	public Ventas() {
		super();
	}

	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vta_id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "vta_comprobante", length = 255)
	private String vtaComprobante;
	
	@Basic(optional = false)
	@Column(name = "vta_fecha")
	private Date vtaFecha;
	
	@Basic(optional = false)
	@Column(name = "vta_corte")
	private Long vtaCorte;
	
	@Basic(optional = false)
	@Column(name = "vta_tercero", length = 255)
	private String vtaTercero;
	
	@Basic(optional = false)
	@Column(name = "vta_credito", length = 255)
	private String vtaCredito;
	
	@Basic(optional = false)
	@Column(name = "vta_cantidad")
	private Long vtaCantidad;
	
	@Basic(optional = false)
	@Column(name = "vta_unidad", length = 255)
	private String vtaUnidad;
	
	@Basic(optional = false)
	@Column(name = "vta_precio")
	private Double vtaPrecio;
	
	@Basic(optional = false)
	@Column(name = "vta_rec_vlor_ttal")
	private Double vtaValorTotal;
	
	@Basic(optional = false)
	@Column(name = "vta_cod_debito", length = 255)
	private String vtaCodDebito;
	
	@Basic(optional = false)
	@Column(name = "vta_estado_cultivo", length = 255)
	private String vtaEstadoCultivo;
	
	@JoinColumn(name = "vta_cc_id", referencedColumnName = "cc_id")
	@ManyToOne
	private CentroDeCosto centroCosto;
	
}
