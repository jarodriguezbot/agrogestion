/**
 * 
 */
package com.sumset.agrogestiondatos.dao.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sumset.agrogestiondatos.dao.interfaces.ICentroDeCostoServiceRepo;
import com.sumset.agrogestiondatos.models.CentroDeCosto;
import com.sumset.agrogestiondatos.operacionesbasicas.impl.OperacionesBasicasImpl;
import com.sumset.agrogestiondatos.repositories.ICentroDeCostoRepo;

/**
 * @author Agustín Palomino Pardo
 *
 */
@Transactional
@Service
public class CentroDeCostoServiceRepo
		extends OperacionesBasicasImpl<CentroDeCosto, Long> implements ICentroDeCostoServiceRepo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	ICentroDeCostoRepo centroDeCostoRepo;
	
	@Override
	public CrudRepository<CentroDeCosto, Long> getRepo() {
		return centroDeCostoRepo;
	}



}
